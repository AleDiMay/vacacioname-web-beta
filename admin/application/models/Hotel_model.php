<?php
class Hotel_model extends CI_Model {

	public function __construct()
    {
      parent::__construct();
    }
   
   	function GetHotels(){
    	$this->load->dbutil();
		  $query = $this->db->query("SELECT * FROM hotels");
      return $query;
   	}

   	function GetDestinations(){
   		$this->load->dbutil();
		  $query = $this->db->query("SELECT * FROM destinos");
      return $query;
   	}
    function GetHotelById($id){
      $this->load->dbutil();
      $query = $this->db->query("SELECT * FROM hoteles WHERE id =".$id);
      return $query;
    }
    function GetRoomsByHotelId($id){
      $hoteles = $this->GetHotelById($id);
      $hotel = $hoteles->result_array();
      foreach ($hotel as $row) {
        $query = $this->db->query("SELECT * FROM habitaciones WHERE id_hotel =".$row["id_hotel"]);
        return $query;
      }
    }
    function GetCategorieValue($id){
      $hoteles = $this->GetHotelById($id);
      $hotel = $hoteles->result_array();
      foreach ($hotel as $row) {
        $cat = $row["categoria_hotel_id"];
        switch ($cat) {
          case 'S1':
            return 0;
          break;
          case 'S15':
            return 1;
          break;
          case 'S2':
            return 2;
          break;
          case 'S25':
            return 3;
          break;
          case 'S3':
            return 4;
          break;
          case 'S35':
            return 5;
          break;
          case 'S4':
            return 6;
          break;
          case 'S45':
            return 7;
          break;
          case 'S5':
            return 8;
          break;
          case 'D1':
            return 9;
          break;
          case 'D2':
            return 10;
          break;
          case 'D3':
            return 11;
          break;
          case 'D4':
            return 12;
          break;
          case 'D5':
            return 13;
          break;
          
          default:
            return 5;
          break;
        }
      }
    }
}
?> 