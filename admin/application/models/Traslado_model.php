<?php

class Traslado_model extends CI_Model {
	public function __construct()
    {
      parent::__construct();
    }
  function GetTraslados(){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM traslados WHERE Borrado != 1");
    return $query;
  }
  function GetPaises(){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM paises_catalogo WHERE Borrado = 0");
    return $query;
  }
  function AddPrecioTraslado($tipotraslado, $pax, $cambio, $traslado, $precio){
    $this->load->dbutil(); 
    $insert = $this->db->query("CALL sp_manage_precio_traslado(".$tipotraslado.", ".$pax.", ".$cambio.", ".$traslado.", ".$precio.")");
    $query = $this->db->insert_id();
    return $insert;
  }
  function AddTraslado($nombre, $zona){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO traslados (Nombre, Zona) VALUES ('".$nombre."', ".$zona.")");
    $query = $this->db->insert_id();
    return $query; 
  }
  function GetPais($id){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM paises_catalogo WHERE Id=".$id);
    return $query;
  }
  function UpdateStatus($id, $estado){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE traslados SET Activo = ".$estado." WHERE Id=".$id);
    $query = $this->db->query("SELECT * FROM traslados WHERE Id=".$id);
    return $query;
  }
  function UpdatePais($id, $nombre){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE paises_catalogo SET Pais = '".$nombre."' WHERE Id=".$id);
    $query = $this->db->query("SELECT * FROM paises_catalogo WHERE Id=".$id);
    return $query;
  }     
  function AddPais($nombre){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO paises_catalogo (Pais) VALUES ('".$nombre."')");
    $query = $this->db->insert_id();
    return $query;  
  }
  function GetEstados(){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM estados_catalogo WHERE Borrado = 0");
    return $query;
  }
  function GetEstadosByPais($pais){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM estados_catalogo WHERE Borrado = 0 AND Pais =".$pais);
    return $query;    
  }
  function AddEstado($nombre, $pais){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO estados_catalogo (Estado, Pais) VALUES ('".$nombre."', ".$pais.")");
    $query = $this->db->insert_id();
    return $query;  
  }
  function GetEstado($id){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM estados_catalogo WHERE Id=".$id);
    return $query;
  }
  function UpdateEstado($id, $nombre, $pais){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE estados_catalogo SET Estado = '".$nombre."', Pais = '".$pais."' WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM estados_catalogo WHERE Id=".$id);
    return $query;
  }
  function AddCiudad($nombre, $estado){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO ciudades_catalogo (Ciudad, Estado) VALUES ('".$nombre."', ".$estado.")");
    $query = $this->db->insert_id();
    return $query;
  }
  function GetCiudad($id){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM ciudades_catalogo WHERE Id=".$id);
    return $query;
  }
  function UpdateCiudad($id, $nombre, $estado){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE ciudades_catalogo SET Ciudad = '".$nombre."', Estado = '".$estado."' WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM ciudades_catalogo WHERE Id=".$id."GROUP BY Estado");
    return $query;
  }
  function GetCiudades(){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM ciudades_catalogo WHERE Borrado = 0");
    return $query;
  }
  function GetCiudadesByEstado($estado){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM ciudades_catalogo WHERE Borrado = 0 AND Estado = ".$estado);
    return $query;
  }
  function AddZona($nombre, $ciudad){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO zonas_catalogo (Nombre_Zona, Ciudad_Zona) VALUES ('".$nombre."', ".$ciudad.")");
    $query = $this->db->insert_id();
    return $query;
  }
  function GetZona($zona){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM vista_zona WHERE Id = ".$zona);
    return $query;
  }
  function GetZonas(){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM vista_zona WHERE Borrado != 1");
    return $query;
  }
  function EditZona($zona, $nombre, $ciudad){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE zonas_catalogo SET Nombre_Zona = '".$nombre."', Ciudad_Zona = '".$ciudad."' WHERE Id=".$zona);
    $query = $this->db->query("SELECT Id FROM zonas_catalogo WHERE Id=".$zona);
    return $query;
  }
  function GetDestinosHoteles(){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM destinos");
    return $query;
  }
  function GetHotelesByDestino($destino){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM vista_hoteles_zona WHERE id_destino = ".$destino);
    return $query;
  }
  function GetHotelesByZona($zona){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM vista_hoteles_por_zona WHERE Zona = ".$zona);
    return $query;
  }
  function AddHotelToZone($hotel, $zona){
    $this->load->dbutil(); 
    $insert = $this->db->query("CALL sp_manage_hotel_zone(".$hotel.",".$zona.")");
    $query = $this->db->insert_id();
    return $query;
  }
  function RemoveHotelFromZone($hotel){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE hoteles_zona_catalogo SET Zona = null WHERE Hotel=".$hotel);
    $query = $this->db->query("SELECT Id FROM hoteles_zona_catalogo WHERE Hotel=".$hotel);
    return $query;
  }
  function GetCurrencies(){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM tipo_cambio");
    return $query;
  }
  function UpdateTipoCambioStatus($id, $estado){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE tipo_cambio SET Activo = ".$estado." WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM tipo_cambio WHERE Id=".$id);
    return $query;
  }
  function GetTiposTraslado(){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM tipo_traslado");
    return $query;
  }
  function AddTrasladoTipo($nombre, $descripción){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO tipo_traslado (Nombre, Descripcion) VALUES ('".$nombre."', '".$descripción."')");
    $query = $this->db->insert_id();
    return $query;
  }
  function AddInclusion($tipo_traslado, $nombre, $activo, $precio){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO inclusiones_traslado (Traslado_Tipo, Inclusion, Activo, Costo_Extra) VALUES (".$tipo_traslado.", '".$nombre."', ".$activo.", ".$precio.")");
    $query = $this->db->insert_id();
    return $query;
  }
  function GetTipoTraslado($id){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM tipo_traslado WHERE Id = ".$id);
    return $query;
  }
  function GetInclusiones($tipo_traslado){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM inclusiones_traslado WHERE Traslado_Tipo = ".$tipo_traslado);
    return $query;
  }
  function UpdateTipoTraslado($id, $nombre, $descripcion){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE tipo_traslado SET Nombre = '".$nombre."', Descripcion = '".$descripcion."' WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM tipo_traslado WHERE Id=".$id);
    return $query;
  }
  function EditInclusiones($id, $inclusion, $activo, $costo){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE inclusiones_traslado SET Inclusion = '".$inclusion."', Activo = ".$activo.", Costo_Extra = ".$costo." WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM inclusiones_traslado WHERE Id=".$id);
    return $query;
  }
  function GetTiposPax(){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM tipo_pax");
    return $query;
  }
  function GetTipoPax($id){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM tipo_pax WHERE Id=".$id);
    return $query;
  }
  function AddTipoPax($nombre, $minimo, $maximo){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO tipo_pax (Nombre, Minimo_Pax, Maximo_Pax) VALUES ('".$nombre."', '".$minimo."', ".$maximo.")");
    $query = $this->db->insert_id();
    return $query;
  }
  function UpdateTipoPax($id, $nombre, $maximo, $minimo){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE tipo_pax SET Nombre = '".$nombre."', Minimo_Pax = ".$maximo.", Maximo_Pax = ".$minimo." WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM tipo_pax WHERE Id=".$id);
    return $query;
  }
  function GetPrecios(){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM precio_traslado");
    return $query;
  }
  function GetPrecio($id){
    $this->load->dbutil(); 
    $query = $this->db->query("SELECT * FROM precio_traslado");
    return $query;
  }
  function AddPrecio($tipo, $pax, $cambio, $traslado, $precio){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO precio_traslado (Tipo_Traslado, Tipo_Pax, Tipo_Cambio, Traslado, Valor_Precio) VALUES (".$tipo.", ".$pax.", ".$cambio.", ".$traslado.", ".$precio.")");
    $query = $this->db->insert_id();
    return $query;
  }
  function EditPrecio($id, $tipo, $pax, $cambio, $traslado, $precio){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE precio_traslado SET Tipo_Traslado = ".$tipo."' Tipo_Pax = ".$pax.", Tipo_Cambio = ".$cambio.", Traslado = ".$traslado.", Valor_Precio = ".$precio." WHERE Id=".$id);
    $query = $this->db->query("SELECT Id FROM precio_traslado WHERE Id=".$id);
    return $query;
  }
  //Deleting Functions
  function DeleteTraslado($traslado){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE traslados SET Borrado = 1 WHERE Id=".$traslado);
    $query = $this->db->query("SELECT Id FROM traslados WHERE Id=".$traslado);
    return $query;
  }
  function DeleteZona($zona){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE zonas_catalogo SET Borrado = 1 WHERE Id=".$zona);
    $query = $this->db->query("SELECT Id FROM zonas_catalogo WHERE Id=".$zona);
    return $query;
  }
  function DeleteTipoPax($tipopax){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE tipo_pax SET Borrado = 1 WHERE Id=".$tipopax);
    $query = $this->db->query("SELECT Id FROM tipo_pax WHERE Id=".$tipopax);
    return $query;
  }
  function DeleteTipoTraslado($tipotraslado){
    $this->load->dbutil();
    $insert = $this->db->query("UPDATE tipo_traslado SET Borrado = 1 WHERE Id=".$tipotraslado);
    $query = $this->db->query("SELECT Id FROM tipo_traslado WHERE Id=".$tipotraslado);
    return $query;
  }
}
?> 