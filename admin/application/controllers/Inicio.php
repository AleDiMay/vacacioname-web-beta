<?php
/*
Controlador General de Traslados
Autor: Alejandro Diaz May
Corp: Vacacioname COM COMMX MX
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Traslado_model', '', TRUE);
		$ciudades = $this->Traslado_model->GetCiudades();
		$ciudadesView = array('rs_ciudades' => $ciudades->result_array());
		$this->load->view('Frontend/General/header');
		$this->load->view('Frontend/General/Nav');
		$this->load->view('Frontend/Inicio/index', $ciudadesView);
		$this->load->view('Frontend/General/footer');
	}
}
?>