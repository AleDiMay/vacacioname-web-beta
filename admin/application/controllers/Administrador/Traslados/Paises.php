<?php
/*
Controlador General de Traslados
Autor: Alejandro Diaz May
Corp: Vacacioname COM COMMX MX
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Traslados extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Traslado_model', '', TRUE);
		$traslados = $this->Traslado_model->GetTraslados();
		//$destinos = $this->Traslado_model->GetDestinations();
		$trasladosView = array('rs_traslados' => $traslados);
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/index', $trasladosView);
		$this->load->view('General/scripts');
	}
	public function edit($id)
	{
		$this->load->model('Traslado_model', '', TRUE);
		$hotel = $this->Traslado_model->GetHotelById($id);
		$categorie = $this->Traslado_model->GetCategorieValue($id);
		$rooms = $this->Traslado_model->GetRoomsByHotelId($id);
		$hotelesView = array('rs_hotel' => $hotel->result_array(), 'categoria' => $categorie, 'habitaciones' => $rooms->result_array());
		$this->load->view('General/header');
		$this->load->view('Hotel/edit', $hotelesView);
		$this->load->view('General/scripts');
	}
	public function Agregar(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["zona"])){
			$response = $this->Traslado_model->AddTraslado($_POST["nombre"], $_POST["zona"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{
			$zonas = $this->Traslado_model->GetZonas();
			$tipoTraslado = $this->Traslado_model->GetTiposTraslado();
			$pax = $this->Traslado_model->GetTiposPax();
			$agregarView = array('rs_zonas' => $zonas->result_array(), 'rs_tipotraslado' => $tipoTraslado->result_array());
			$moduleEspecials = array('especial' => 'traslado', 'rs_pax' => $pax->result_array());
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Nuevo', $agregarView);
			$this->load->view('General/scripts', $moduleEspecials);
		}
	}
	public function AgregarPrecioATraslado(){
		$this->load->model('Traslado_model', '', TRUE);
		$response = $this->Traslado_model->AddPrecioTraslado($_POST["tipo"], $_POST["pax"], $_POST["cambio"], $_POST["traslado"], $_POST["precio"]);
		$responseView = array('rs' => $response);
		$this->load->view('General/response2', $responseView);
	}
	public function Paises()
	{
		$this->load->model('Traslado_model', '', TRUE);
		$paises = $this->Traslado_model->GetPaises();
		$paisesView = array('rs_paises' => $paises);
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/Paises/index', $paisesView);
		$this->load->view('General/scripts');		
	}
	public function EditarPais($id)
	{
		$this->load->model('Traslado_model', '', TRUE);
		$pais = $this->Traslado_model->GetPais($id);
		$paisView = array('rs_pais' => $pais);
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/Paises/Editar', $paisView);
		$this->load->view('General/scripts');		
	}
	public function UpdateTrasladoStatus()
	{
		$id = isset($_POST["id"])?$_POST["id"]:null;
		$estado = isset($_POST["estado"])?$_POST["estado"]:null;
		$this->load->model('Traslado_model', '', TRUE);
		$traslados = $this->Traslado_model->UpdateStatus($id, $estado);
		$trasladosView = array('rs' => $traslados->result_array());
		$this->load->view('General/response', $trasladosView);
	}
	public function UpdatePais(){
		$id = isset($_POST["id"])?$_POST["id"]:null;
		$nombre = isset($_POST["nombre"])?$_POST["nombre"]:null;
		$this->load->model('Traslado_model', '', TRUE);
		$response = $this->Traslado_model->UpdatePais($id, $nombre);
		$responseView = array('rs' => $response->result_array());
		$this->load->view('General/response', $responseView);
	}
	public function AgregarPais(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"])){
			$response = $this->Traslado_model->AddPais($_POST["nombre"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/responseOK', $responseView);
		}else{				
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Paises/Nuevo');
			$this->load->view('General/scripts');
		}
	}
	public function Estados(){
		$this->load->model('Traslado_model', '', TRUE);
		$estados = $this->Traslado_model->GetEstados();
		$paises = $this->Traslado_model->GetPaises();
		$estadosView = array('rs_estados' => $estados->result_array(), 'rs_paises' => $paises->result_array());
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/Estados/index', $estadosView);
		$this->load->view('General/scripts');	
	}
	public function ListaEstadosPorPais($pais){
		$this->load->model('Traslado_model', '', TRUE);
		$estados = $this->Traslado_model->GetEstadosByPais($pais);
		$estadosView = array('rs_estados' => $estados->result_array(), 'rs_count' => $estados->num_rows());
		$this->load->view('Administrador/Traslados/Estados/Lista', $estadosView);
	}
	public function AgregarEstado(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["pais"])){
			$response = $this->Traslado_model->AddEstado($_POST["nombre"],$_POST["pais"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{	
			$paises = $this->Traslado_model->GetPaises();
			$responseView = array('rs' => $paises->result_array());			
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Estados/Nuevo', $responseView);
			$this->load->view('General/scripts');
		}
	}
	public function EditarEstado($id){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["pais"])){
			$response = $this->Traslado_model->UpdateEstado($id, $_POST["nombre"], $_POST["pais"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/responseOK', $responseView);
		}else{
			$estado = $this->Traslado_model->GetEstado($id);
			$paises = $this->Traslado_model->GetPaises();
			$estadoView = array('rs_estado' => $estado->result_array(), 'rs' => $paises->result_array());
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Estados/Editar', $estadoView);
			$this->load->view('General/scripts');
		}
	}
	public function AgregarCiudad(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["estado"])){
			$response = $this->Traslado_model->AddCiudad($_POST["nombre"],$_POST["estado"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{	
			$estados = $this->Traslado_model->GetEstados();
			$responseView = array('rs' => $estados->result_array());			
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Ciudades/Nuevo', $responseView);
			$this->load->view('General/scripts');
		}
	}
	public function EditarCiudad($id){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["estado"])){
			$response = $this->Traslado_model->UpdateCiudad($id, $_POST["nombre"], $_POST["estado"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/responseOK', $responseView);
		}else{
			$ciudad = $this->Traslado_model->GetCiudad($id);
			$estados = $this->Traslado_model->GetEstados();
			$ciudadView = array('rs_ciudad' => $ciudad->result_array(), 'rs' => $estados->result_array());
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Ciudades/Editar', $ciudadView);
			$this->load->view('General/scripts');
		}
	}
	public function Ciudades(){
		$this->load->model('Traslado_model', '', TRUE);
		$ciudades = $this->Traslado_model->GetCiudades();
		$estados = $this->Traslado_model->GetEstados();
		$ciudadesView = array('rs_ciudades' => $ciudades->result_array(), 'rs_estados' => $estados->result_array());
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/Ciudades/index', $ciudadesView);
		$this->load->view('General/scripts');
	}
	public function ListaCiudadesPorEstado($estado){
		$this->load->model('Traslado_model', '', TRUE);
		$ciudades = $this->Traslado_model->GetCiudadesByEstado($estado);
		$ciudadesView = array('rs_ciudades' => $ciudades->result_array(), 'rs_count' => $ciudades->num_rows());
		$this->load->view('Administrador/Traslados/Ciudades/Lista', $ciudadesView);
	}
	public function Zonas(){
		$this->load->model('Traslado_model', '', TRUE);
		$zonas = $this->Traslado_model->GetZonas();
		$ciudades = $this->Traslado_model->GetCiudades();
		$zonasView = array('rs_zonas' => $zonas->result_array(), 'rs_ciudades' => $ciudades->result_array());
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/Zonas/index', $zonasView);
		$this->load->view('General/scripts');
	}
	public function AgregarZona(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["ciudad"])){
			$response = $this->Traslado_model->AddZona($_POST["nombre"], $_POST["ciudad"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{
			$paises = $this->Traslado_model->GetPaises();
			$destinos = $this->Traslado_model->GetDestinosHoteles();
			$responseView = array('rs_paises' => $paises->result_array(), 'rs_destinos' => $destinos->result_array());			
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Zonas/Nuevo', $responseView);
			$this->load->view('General/scripts');
		}
	}
	public function EditarZona($id){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["ciudad"])){
			$response = $this->Traslado_model->EditZona($id, $_POST["nombre"],$_POST["ciudad"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{
			$zona = $this->Traslado_model->GetZona($id);
			foreach ($zona->result_array() as $zonaR) {				
				$ciudades = $this->Traslado_model->GetCiudadesByEstado($zonaR["Estado"]);
				$estados = $this->Traslado_model->GetEstadosByPais($zonaR["Pais"]);
			}
			$destinos = $this->Traslado_model->GetDestinosHoteles();			
			$paises = $this->Traslado_model->GetPaises();
			$hoteles = $this->Traslado_model->GetHotelesByZona($id);
			$responseView = array(
				'rs_paises' 	=> $paises->result_array(), 
				'rs_destinos' 	=> $destinos->result_array(),
				'rs_ciudades' 	=> $ciudades->result_array(),
				'rs_estados' 	=> $estados->result_array(),
				'rs_hoteles' 	=> $hoteles->result_array(),
				'rs_zona'		=> $zona->result_array()
			);			
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/Zonas/Editar', $responseView);
			$this->load->view('General/scripts');
		}
	}
	public function AgregarHotelAZona(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["hotel"]) && isset($_POST["zona"])){
			$response = $this->Traslado_model->AddHotelToZone($_POST["hotel"],$_POST["zona"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}
	}
	public function ListaHotelesPorDestino($destino, $zona){
		$this->load->model('Traslado_model', '', TRUE);
		$hoteles = $this->Traslado_model->GetHotelesByDestino($destino);
		$hotelesView = array('rs_hoteles' => $hoteles->result_array(), 'rs_count' => $hoteles->num_rows(), 'zona' => $zona);
		$this->load->view('Administrador/Traslados/Zonas/Lista', $hotelesView);
	}
	public function RemoverHotelZona(){
		$this->load->model('Traslado_model', '', TRUE);
		$response = $this->Traslado_model->RemoveHotelFromZone($_POST["hotel"]);
		$responseView = array('rs' => $response);
		$this->load->view('General/response2', $responseView);
	}
	public function TipoCambio(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["Id"]) && isset($_POST["Estado"])){
			$response = $this->Traslado_model->UpdateTipoCambioStatus($_POST["Id"], $_POST["Estado"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{
			$currency = $this->Traslado_model->GetCurrencies();
			$currencyView = array('rs_currency' => $currency->result_array());
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/TipoCambio/index', $currencyView);
			$this->load->view('General/scripts');
		}
	}
	public function AgregarTipoTraslado(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["descripcion"])){
			$response = $this->Traslado_model->AddTrasladoTipo($_POST["nombre"], $_POST["descripcion"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{
			$moduleEspecials = array('especial' => 'tipotraslado');
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/TipoTraslado/Nuevo');
			$this->load->view('General/scripts', $moduleEspecials);
		}
	}
	public function AgregarInclusion($tipo_traslado){
		$this->load->model('Traslado_model', '', TRUE);
		$response = $this->Traslado_model->AddInclusion($tipo_traslado, $_POST["nombre"], $_POST["activo"], $_POST["costo"]);
		$responseView = array('rs' => $response);
		$this->load->view('General/response2', $responseView);
	}
	public function TipoTraslado(){
		$this->load->model('Traslado_model', '', TRUE);
		$tipoTraslado = $this->Traslado_model->GetTiposTraslado();
		$tipoTrasladoView = array('rs_tipotraslado' => $tipoTraslado->result_array());
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/TipoTraslado/index', $tipoTrasladoView);
		$this->load->view('General/scripts');
	}
	public function EditarTipoTraslado($tipo_traslado){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["descripcion"])){
			$response = $this->Traslado_model->UpdateTipoTraslado($tipo_traslado, $_POST["nombre"], $_POST["descripcion"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/responseOK', $responseView);
		}else{
			$tipoTraslado = $this->Traslado_model->GetTipoTraslado($tipo_traslado);
			$inclusiones = $this->Traslado_model->GetInclusiones($tipo_traslado);
			$tipoTrasladoView = array('rs_tipoTraslado' => $tipoTraslado->result_array(), 'rs_inclusiones' => $inclusiones->result_array());
			$moduleEspecials = array('especial' => 'tipotraslado');
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/TipoTraslado/Editar', $tipoTrasladoView);
			$this->load->view('General/scripts', $moduleEspecials);
		}
	}
	public function EditarInclusion($id){
		$this->load->model('Traslado_model', '', TRUE);
		$response = $this->Traslado_model->EditInclusiones($id, $_POST["nombre"], $_POST["activo"], $_POST["costo"]);
		$responseView = array('rs' => $response);
		$this->load->view('General/response2', $responseView);
	}
	public function TipoPax(){
		$this->load->model('Traslado_model', '', TRUE);
		$tipopax = $this->Traslado_model->GetTiposPax();
		$tipopaxView = array('rs_tipopax' => $tipopax->result_array());
		$this->load->view('General/header');
		$this->load->view('Administrador/Traslados/TipoPax/index', $tipopaxView);
		$this->load->view('General/scripts');
	}
	public function ListaOptionsTipoPax(){
		$this->load->model('Traslado_model', '', TRUE);
		$tipopax = $this->Traslado_model->GetTiposPax();
		$tipopaxView = array('rs_pax' => $tipopax->result_array());
		$this->load->view('Administrador/Traslados/TipoPax/ListaOptions', $tipopaxView);
	}
	public function ListaOptionsTipoCambio(){
		$this->load->model('Traslado_model', '', TRUE);
		$tipocambio = $this->Traslado_model->GetCurrencies();
		$tipocambioView = array('rs_tipocambio' => $tipocambio->result_array());
		$this->load->view('Administrador/Traslados/TipoCambio/ListaOptions', $tipocambioView);
	}
	public function AgregarTipoPax(){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["minimo"]) && isset($_POST["maximo"])){
			$response = $this->Traslado_model->AddTipoPax($_POST["nombre"], $_POST["minimo"], $_POST["maximo"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/response2', $responseView);
		}else{
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/TipoPax/Nuevo');
			$this->load->view('General/scripts');
		}
	}
	public function EditarTipoPax($id){
		$this->load->model('Traslado_model', '', TRUE);
		if(isset($_POST["nombre"]) && isset($_POST["minimo"]) && isset($_POST["maximo"])){
			$response = $this->Traslado_model->UpdateTipoPax($id, $_POST["nombre"], $_POST["minimo"], $_POST["maximo"]);
			$responseView = array('rs' => $response);
			$this->load->view('General/responseOK', $responseView);
		}else{
			$tipopax = $this->Traslado_model->GetTipoPax($id);
			$tipopaxView = array('rs_tipopax' => $tipopax->result_array());
			$this->load->view('General/header');
			$this->load->view('Administrador/Traslados/TipoPax/Editar', $tipopaxView);
			$this->load->view('General/scripts');
		}
	}
}
?>