<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div class="user-details" style="background: url('http://localhost/assets/images/fondo.png')">
                        <div class="pull-left">
                            <img src="http://localhost/assets/images/perfil.png" alt="" class="thumb-md img-circle">
                        </div>
                        <div class="user-info">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#fff;" aria-expanded="false">Vacacioname <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)"><i class="zmdi zmdi-face-unlock"></i> Perfil<div class="ripple-wrapper"></div></a></li>
                                    <li><a href="javascript:void(0)"><i class="zmdi zmdi-settings"></i> Configuraciones</a></li>
                                    <li><a href="javascript:void(0)"><i class="zmdi zmdi-lock"></i> Bloquear</a></li>
                                    <li><a href="javascript:void(0)"><i class="zmdi zmdi-settings-power"></i> Cerrar Sesión</a></li>
                                </ul>
                            </div>
                            
                            <p class="text-muted m-0" style="color:#ccc;">Administrador</p>
                        </div>
                    </div>
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            <li>
                                <a href="index.html" class="waves-effect"><i class="zmdi zmdi-home"></i><span> Inicio </span></a>
                            </li>
                            <!--Inicio Hoteles-->
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="zmdi zmdi-sun"></i> <span> Tours</span> <span class="pull-right"><i class="zmdi zmdi-plus"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="typography.html">Todos los Hoteles</a></li>
                                    <li><a href="typography.html">Agregar Nuevo</a></li>
                                </ul>
                            </li>
                            <!--Fin hoteles-->
                            <!--Inicio Traslados-->
                            <li class="has_sub">
                                <a href="#" class="waves-effect active"><i class="zmdi zmdi-car-taxi"></i> <span> Traslados</span> <span class="pull-right"><i class="zmdi zmdi-plus"></i></span></a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/">
                                            Traslados
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/Paises">
                                            Paises
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/Estados">
                                            Estados</a>
                                        </li>
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/Ciudades">
                                            Ciudades
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/Zonas">
                                            Zonas
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/TipoPax">
                                            TIpos de Pax
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $GLOBALS['pathgeneral'] ?>admin/index.php/Administrador/Traslados/TipoTraslado">
                                            TIpos de Traslado
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>