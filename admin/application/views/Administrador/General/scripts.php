<?php
    $cdn = "http://goguytravel.com/assets/admin/";    
?>
<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo $cdn ?>/js/jquery.min.js"></script>
        <script src="<?php echo $cdn ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo $cdn ?>/js/waves.js"></script>
        <script src="<?php echo $cdn ?>/js/wow.min.js"></script>
        <script src="<?php echo $cdn ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo $cdn ?>/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo $cdn ?>/assets/chat/moment-2.2.1.js"></script>
        <script src="<?php echo $cdn ?>/assets/jquery-sparkline/jquery.sparkline.min.js"></script>
        <script src="<?php echo $cdn ?>/assets/jquery-detectmobile/detect.js"></script>
        <script src="<?php echo $cdn ?>/assets/fastclick/fastclick.js"></script>
        <script src="<?php echo $cdn ?>/assets/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="<?php echo $cdn ?>/assets/jquery-blockui/jquery.blockUI.js"></script>

        <!-- sweet alerts -->
        <script src="<?php echo $cdn ?>/assets/sweet-alert/sweet-alert.min.js"></script>
        <script src="<?php echo $cdn ?>/assets/sweet-alert/sweet-alert.init.js"></script>

        <!-- flot Chart -->
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.time.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.resize.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.pie.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.selection.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.stack.js"></script>
        <script src="<?php echo $cdn ?>/assets/flot-chart/jquery.flot.crosshair.js"></script>

        <!-- Counter-up -->
        <script src="<?php echo $cdn ?>/assets/counterup/waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo $cdn ?>/assets/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        
        <!-- CUSTOM JS -->
        <script src="<?php echo $cdn ?>/js/jquery.app.js"></script>

        <!-- Dashboard -->
        <script src="<?php echo $cdn ?>/js/jquery.dashboard.js"></script>

        <!-- Chat -->
        <script src="<?php echo $cdn ?>/js/jquery.chat.js"></script>

        <!-- Todo -->
        <script src="<?php echo $cdn ?>/js/jquery.todo.js"></script>

        <script type="text/javascript">
            /* ==============================================
            Counter Up
            =============================================== */
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });                
            });            
        </script>
        
        <script src="<?php echo $cdn ?>/assets/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo $cdn ?>/assets/datatables/dataTables.bootstrap.js"></script>

        <!-- ION Sliders-->
        <!--<script src="http://localhost/assets/ion-rangeslider/ion.rangeSlider.min.js"></script>
        <script src="http://localhost/assets/ion-rangeslider/ui-sliders.js"></script>-->

        <script src="<?php echo $cdn ?>/assets/timepicker/bootstrap-timepicker.min.js"></script>
        
        
        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
            } );
        </script>

        <script type="text/javascript" src="<?php echo $cdn ?>/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="<?php echo $cdn ?>/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

        <!--form validation init-->
        <script src="<?php echo $cdn ?>/assets/summernote/summernote.min.js"></script>

        <!-- Examples -->
        <script src="<?php echo $cdn ?>/assets/magnific-popup/magnific-popup.js"></script>
        <script src="<?php echo $cdn ?>/assets/jquery-datatables-editable/jquery.dataTables.js"></script>

        <?php 
            if(isset($especial)){
                switch ($especial) {
                    case 'traslado':
                    ?>
                        <script src="<?php echo $cdn ?>/assets/jquery-datatables-editable/datatables.editable.traslado.init.js"></script>
                    <?php                        
                    break;
                    case 'tipotraslado'
                    ?>                        
                        <script src="<?php echo $cdn ?>/assets/jquery-datatables-editable/datatables.editable.init.js"></script>
                    <?php 
                    break;                   
                    default:
                        # code...
                    break;
                  }  
            }
        ?>

        <script>

            jQuery(document).ready(function(){
                $('.wysihtml5').wysihtml5();

                $('.summernote').summernote({
                    height: 100,
                    toolbar: [
                                // [groupName, [list of button]]
                                ['style', ['bold', 'italic', 'underline', 'clear']],
                                ['font', ['strikethrough']],
                                ['para', ['ul', 'ol', 'paragraph']]
                              ]               // set focus to editable area after initializing summernote
                });

            });
        </script>

        <script>
            jQuery(document).ready(function() {                    
                // Time Picker
                jQuery('#timepicker').timepicker({defaultTIme: false});
                  
            });
        </script>
        
    </body>
</html>