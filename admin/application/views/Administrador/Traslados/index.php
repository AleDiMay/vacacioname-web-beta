	<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->
            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Traslados</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Go Guy Travel</a></li>
                                    <li><a href="#">Tables</a></li>
                                    <li class="active">Traslados</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title col-md-6">Traslados</h3>
                                        <div class="col-md-6">
                                            <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/Agregar" class="btn btn-primary pull-right waves-effect waves-light">Agregar Traslado <i class="fa fa-plus"></i></a>
                                        </div>
                                        <div class="col-md-12" style="margin-top: 20px;">
                                            <div class="alert alert-info" role="alert">
                                                Para realizar correctamente la captura de un traslado es necesario haber creado con anterioridad <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarTipoTraslado">El tipo de Traslado</a>, <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarTipoPax">Tipo de Paquete de Pasajeros</a> y <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarZona">Zona del Traslado</a>, en caso de no haber realizado la previa captura de alguno de estos requerimientos haga click sobre el para dirigirse a la pagina de captura del mismo.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Traslado</th>
                                                            <th>Zona</th>
                                                            <th>Activo</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>                                             
                                                    <tbody>
                                                    <?php foreach ($rs_traslados->result_array() as $row){ ?>
                                                        <tr id="row<?php echo $row["Id"]; ?>">
                                                            <td class="col-md-5" id="name<?php echo $row["Id"]; ?>">
                                                                <?php echo $row["Nombre"]; ?>                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <?php echo $row["Zona"]; ?>
                                                            </td>
                                                            <td class="col-md-2">
                                                                <input type="checkbox" id="check<?php echo $row["Id"]; ?>" name="my-checkbox">
                                                            </td>
                                                            <td class="col-md-3">
                                                                <div class="btn-group" role="group">
                                                                  <a href="Traslados/Editar/<?php echo $row["Id"]; ?>" type="button" class="btn btn-primary btn-small"><i class="zmdi zmdi-edit"></i> Editar</a>
                                                                  <button type="button" id="<?php echo $row['Id']; ?>" class="btn eliminar btn-danger"><i class="zmdi zmdi-delete" data-traslado="<?php echo $row["Id"]; ?>"></i> Eliminar</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <script>
                                                        $("[id='check<?php echo $row["Id"] ?>']").bootstrapSwitch({
                                                                <?php
                                                                    if($row["Activo"]==1){echo "state: true,";}
                                                                    else{echo "state: false,";}
                                                                ?>                 
                                                                size: "small",
                                                                onText: "Activo",
                                                                offText: "Inactivo",
                                                                onColor: "success",
                                                                offColor: "danger",
                                                                labelText: "Estado",
                                                                onSwitchChange(event, state){
                                                                    console.log(state);
                                                                    if(state){
                                                                        var estatus = 1;
                                                                    }else{
                                                                        var estatus = 0;
                                                                    }

                                                                    $.ajax({
                                                                        url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/UpdateTrasladoStatus', 
                                                                        data : {
                                                                            id : <?php echo $row["Id"] ?>,
                                                                            estado : estatus
                                                                        }, 
                                                                        type : 'POST',
                                                                        dataType : 'json',
                                                                        success : function(data) {
                                                                            Materialize.toast('Cambios Guardados Correctamente', 4000);
                                                                        },
                                                                        error : function(xhr, status) {
                                                                            Materialize.toast('Ocurrio un error guardando la información', 4000);
                                                                        },
                                                                        complete : function(xhr, status) {
                                                                            
                                                                        }
                                                                    });
                                                                }
                                                            });

                                                        </script>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->
                </div> <!-- content -->
                <footer class="footer text-right">
                    <?php echo date('Y'); ?> © Go Guy Travel.
                </footer>
            </div>
        </div>
        <script>
       $(".eliminar").click(function(){
            var id = $(this).attr("id");
            console.log(id);
            swal({   
                title: "Eliminar ",   
                text: "¿Esta seguro de que desea eliminar permanentemente este elemento?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonText: "Eliminar",   
                cancelButtonText: "Cancelar",   
                closeOnConfirm: false,   
                closeOnCancel: true 
                }, function(isConfirm){
                    console.log("Eliminar");   
                    if (isConfirm) {     
                        $.ajax({
                        url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EliminarTraslado/'+id, 
                        type : 'POST',
                        dataType : 'json',
                        success : function(data) {
                            swal("Eliminado", "Este elemento ha sido elminado de manera permanente", "success");
                            $("#row"+id).remove();
                        },
                        error : function(xhr, status) {
                            Materialize.toast('Ocurrio un error guardando la información', 4000);
                        },
                        complete : function(xhr, status) {
                                                         
                        }
                    });
                    } else {     
                        
                    } 
                });
        });
        </script>     