<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Agregar Zona</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Maravilla Maya</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Zonas</a></li>
                                    <li class="active">Nueva Zona</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Nueva Zona</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-horizontal col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Nombre de la Zona
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="nombreZona" placeholder="Nombre de la Zona">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Pais
                                                        </label>
                                                        <div class="col-md-8">
                                                            <select class="form-control" id="pais">
                                                                <option value="0">Selecciona un Pais</option>
                                                            <?php foreach ($rs_paises as $row){ ?>
                                                                <option value='<?php echo $row["Id"];?>'>
                                                                    <?php echo $row["Pais"];?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>        
                                                        </div>
                                                        <div id="estados">
                                                            <!--Select Estados-->
                                                        </div>
                                                        <div id="ciudades">
                                                            <!--Select Estados-->
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-6 checkbox checkbox-primary" style="height: 200px; overflow: auto;">
                                                    <ul id="listahoteles" style="list-style-type:none;">

                                                    </ul>
                                                </div>
                                                <div class="col-md-6" style="height: 200px; overflow: auto;">
                                                    <ul id="hotelesactivos" style="list-style-type:none;">
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <button id="saveBtn" class="btn btn-primary">
                                               <i class="zmdi zmdi-floppy"></i> Guardar
                                            </button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->                               
                </div> <!-- content -->
                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>
        </div>   
        <script type="text/javascript">
        $("#pais").change(function(){
            if($( "#pais" ).val() > 0){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaEstadosPorPais/'+$( "#pais" ).val(), 
                    type : 'GET',
                    success : function(data) {
                            console.log(data);
                            $( "#estados" ).html(data);                        
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
               $( "#estados" ).html("");  
               $( "#ciudades" ).html(""); 
            }
        });
        $("#asigner").change(function(){
            if($( "#ciudad" ).val() > 0){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaHotelesPorDestino/'+$( "#ciudad" ).data("destino")+'/0', 
                    type : 'GET',
                    success : function(data) {
                            console.log(data);
                            $( "#listahoteles" ).html(data);                        
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
               $( "#listahoteles" ).html("");
            }
        });
        function GetHoteles(destino){
            $( "#listahoteles" ).html("<div class='col-lg-12 center-block text-center'style='display:flex;justify-content:center;align-content:center;flex-direction:column;height:200px;'><p><b style='margin-top:-10px'>Obteniendo Hoteles </b><i class='fa fa-circle-o-notch fa-spin fa-fw'></i></p></div>");
            console.log("Ejecuta");
            if($( destino ).val() > 0){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaHotelesPorDestino/'+$( "#destinyOpt"+$( destino ).val() ).data("destino")+'/0', 
                    type : 'GET',
                    success : function(data) {
                            //console.log(data);
                            $( "#listahoteles" ).html("");
                            $( "#listahoteles" ).html(data);                        
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
               $( "#listahoteles" ).html("");
            }
        };    
        
        $( "#saveBtn" ).click(function() {
            if($( "#ciudad" ).val() != null){
                console.log($( "#ciudad" ).val());
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarZona', 
                    data : {
                        nombre : $( "#nombreZona" ).val(),
                        ciudad : $( "#ciudad" ).val()
                    }, 
                    type : 'POST',
                    success : function(data) {
                            $( ".hoteldropoff" ).each(function(){
                                $.ajax({
                                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarHotelAZona', 
                                    data : {
                                        hotel : $(this).attr("id"),
                                        zona : data.replace(" ","")
                                    }, 
                                    type : 'POST',
                                    success : function(data) {
                                            
                                    },
                                    error : function(xhr, status) {
                                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                                    },
                                    complete : function(xhr, status) {
                                    }
                                });
                            });
                            //console.log(data);
                            swal({   
                                title: "Guardado!",   
                                text: "Los cambios fueron correctamente Guardados ¿Desea continuar en esta pagina o regresar al listado general?",   
                                type: "success",   
                                showCancelButton: true,   
                                confirmButtonText: "Ir al Listado",   
                                cancelButtonText: "Permanecer",   
                                closeOnConfirm: false,   
                                closeOnCancel: true 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/Zonas');
                                } else {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarZona/'+data.replace(" ","")); 
                                } 
                            });
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
                swal("Error", "Por favor, seleccione un Pais, Estado y Ciudad.", "error"); 
            }                        
        });      
        </script>