<?php
if($rs_count > 0){
	foreach ($rs_hoteles as $row) {
		if(!is_null($row["Zona"])){
			if($row["Zona"]!=$zona){
				echo "<li><input type='checkbox' class='checkboxhotel asignado' value='".$row["Id"]."' id='hotel".$row["Id"]."' checked='' disabled=''><label for='hotel".$row["Id"]."' id='nombre".$row["Id"]."' class='asignado'> ".$row["nombre_hotel"]." (Asignado)</label></li>";
			}else{
				echo "<li><input type='checkbox' class='checkboxhotel' value='".$row["Id"]."' id='hotel".$row["Id"]."' checked=''><label for='hotel".$row["Id"]."' id='nombre".$row["Id"]."'> ".$row["nombre_hotel"]."</label></li>";
			}
		}else{
			echo "<li><input type='checkbox' class='checkboxhotel' value='".$row["Id"]."' id='hotel".$row["Id"]."'><label for='hotel".$row["Id"]."' id='nombre".$row["Id"]."'> ".$row["nombre_hotel"]. "</label></li>";
		}
	}
?>
<script>
	$(".checkboxhotel").change(
        function(
        	){
        	//console.log($(this).attr("class"));
        	if($(this).prop( "checked" )){
        		var id = "#nombre"+$(this).val()
        		$( "#hotelesactivos" ).append("<li id='element"+$(this).val()+"'><span class='hoteldropoff' id='"+$(this).val()+"'>"+$(id).text()+"</span><i class='zmdi zmdi-delete pull-right' id='deleter"+$(this).val()+"' data-hotel='"+$(this).val()+"' style='cursor:pointer' onclick='Erase(this);'></i></li>");
        	}else{
        		$( "#element"+$(this).val() ).remove();
        	}       	
        }
    );
    $( ".asignado" ).click(
    	function(){
    			var id = "#"+$( this ).attr("for");
    			var idthis = "#"+$( this ).attr("id");
//console.log(id);
    		    swal({   
		            title: "Hotel ya Asignado",   
		            text: "Este hotel se encuentra actualmente asignado a una zona, ¿deseas asignarlo a la zona actual? sera removido de la zona en la que se encuentra.",   
		            type: "warning",   
		            showCancelButton: true,   
		            confirmButtonColor: "#DD6B55",   
		            confirmButtonText: "Si, Agregar a esta zona",   
		            cancelButtonText: "No, Mantenerlo en su zona",   
		            closeOnConfirm: false,   
		            closeOnCancel: false 
		        }, function(isConfirm){   
		            if (isConfirm) {
		    			swal("Agregado!", "el Hotel fue correctamente agregado a esta zona.", "success");
		    			$( id ).removeAttr("disabled"); 
		    			$( id ).removeClass( "asignado" );
		    			$( idthis ).removeClass( "asignado" );
		    			$( idthis ).replace( " (Asignado)","" );
		            } else {     
		                swal("Cancelado", "El Hotel se conserva en la zona asignada", "error");   
		            } 
		        });
var classe = $(idthis).attr("class");
    			if(classe == "asignado"){
	    			swal({   
			            title: "Hotel ya Asignado",   
			            text: "Este hotel se encuentra actualmente asignado a una zona, ¿deseas asignarlo a la zona actual? sera removido de la zona en la que se encuentra.",   
			            type: "warning",   
			            showCancelButton: true,   
			            confirmButtonColor: "#DD6B55",   
			            confirmButtonText: "Si, Agregar a esta zona",   
			            cancelButtonText: "No, Mantenerlo en su zona",   
			            closeOnConfirm: false,   
			            closeOnCancel: false 
			        }, function(isConfirm){   
			            if (isConfirm) {
			    			swal("Agregado!", "Ahora este hotel puede ser asignado a esta zona.", "success");
			    			$( id ).removeAttr("disabled"); 
			    			$( id ).removeAttr("checked"); 
			    			$( id ).removeClass( "asignado" );
			    			$( idthis ).removeClass( "asignado" ); 
			    			$( idthis ).removeAttr("class"); 
			    			$( idthis ).text( $(idthis).text().replace(" (Asignado)","") );
			            } else {     
			                swal("Cancelado", "El Hotel se conserva en la zona asignada", "error");   
			            } 
			        });
			    }
}
    );
    function Erase(elem){
    		swal({   
			    title: "Desea Borrar este Hotel?",   
			    text: "Este hotel se encuentra actualmente en la lista de hoteles de esta zona ¿Esta seguro de que desea Eliminarlo?",   
			    type: "warning",   
			    showCancelButton: true,   
			    confirmButtonColor: "#DD6B55",   
			    confirmButtonText: "Si, Eliminar",   
			    cancelButtonText: "No, Mantener",   
			    closeOnConfirm: false,   
			    closeOnCancel: false 
			}, function(isConfirm){   
			    if (isConfirm) {
					$.ajax({
	                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/RemoverHotelZona/', 
                    	data : {
                        	hotel : $(elem).data("hotel")
                    	}, 
	                    type : 'POST',
	                    success : function(datas) {
	                    	swal("Borrado!", "Este Hotel ha sido correctamente eliminado de la lista de hoteles de esta Zona.", "success");
							$( "#hotel"+$(elem).data("hotel") ).prop("checked", false);
							$( "#element"+$(elem).data("hotel") ).remove();                     
	                    },
	                    error : function(xhr, status) {
	                        swal("Error", " ", "error"); 
	                    },
	                    complete : function(xhr, status) {
	                    }
	                });
			    } else {     
			        swal("Cancelado", "El Hotel se conserva en la zona asignada", "error");   
			    } 
			});
    };
</script>
<?php
}