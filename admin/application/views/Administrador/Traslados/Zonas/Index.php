    <script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content --> 
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Catalogo de Zonas</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Zonas</a></li>
                                    <li class="active">Todas los Zonas</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title col-md-6">Zonas</h3>
                                        <div class="col-md-6">
                                            <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarZona" class="btn btn-primary pull-right waves-effect waves-light">Agregar Zona <i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                           
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Zona</th>
                                                            <th>Ciudad</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tb
                                                    ody>
                                                    <?php foreach ($rs_zonas as $zona){ ?>
                                                        <tr>
                                                            <td class="col-md-7">
                                                                <?php echo $zona["Nombre_Zona"]; ?>                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <?php
                                                                    foreach ($rs_ciudades as $ciudad){
                                                                        if($zona["Ciudad"] == $ciudad["Id"]){
                                                                            echo $ciudad["Ciudad"];
                                                                        }
                                                                    }
                                                                ?>                
                                                            </td>
                                                            <td class="col-md-3">
                                                                <div class="btn-group" role="group">
                                                                  <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarZona/<?php echo $zona["Id"]; ?>" type="button" class="btn btn-primary btn-small"><i class="zmdi zmdi-edit"></i> Editar</a>
                                                                  <button type="button" class="btn eliminar btn-danger" id="<?php echo $zona['Id']; ?>"><i class="zmdi zmdi-delete"></i> Eliminar</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer text-right">
                    2015 © Go Guy Travel.
                </footer>

            </div>
        </div>
        <script>
        $(".eliminar").click(function(){
            var id = $(this).attr("id");
            console.log(id);
            swal({   
                title: "Eliminar ",   
                text: "¿Esta seguro de que desea eliminar permanentemente este elemento?",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonText: "Eliminar",   
                cancelButtonText: "Cancelar",   
                closeOnConfirm: false,   
                closeOnCancel: true 
                }, function(isConfirm){
                    console.log("Eliminar");   
                    if (isConfirm) {     
                        $.ajax({
                        url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EliminarZona/'+id, 
                        type : 'POST',
                        dataType : 'json',
                        success : function(data) {
                            swal("Eliminado", "Este elemento ha sido elminado de manera permanente", "success");
                            $("#row"+id).remove();
                        },
                        error : function(xhr, status) {
                            Materialize.toast('Ocurrio un error guardando la información', 4000);
                        },
                        complete : function(xhr, status) {
                                                         
                        }
                    });
                    } else {     
                        
                    } 
                });
        });
        </script> 