<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
    <body class="fixed-left">
        <!-- Begin page -->
        <?php
            foreach ($rs_zona as $zona) {
                }
        ?>
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title"><?php echo $zona["Nombre_Zona"] ?></h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Zonas</a></li>
                                    <li class="active"><?php echo $zona["Nombre_Zona"] ?></li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Editar Zona</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-horizontal col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Nombre de la Zona
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="nombreZona" value="<?php echo $zona['Nombre_Zona'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Pais
                                                        </label>
                                                        <div class="col-md-8">
                                                            <select class="form-control" id="pais">
                                                                <option value="0">Selecciona un Pais</option>
                                                            <?php foreach ($rs_paises as $pais){ ?>
                                                                <?php
                                                                $selectPais = $pais["Id"]==$zona["Pais"]?"selected=selected":"";
                                                                ?>
                                                                <option value='<?php echo $pais["Id"];?>' <?php echo $selectPais ?> >
                                                                    <?php echo $pais["Pais"];?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>        
                                                        </div>
                                                        <div id="estados">
                                                            <label class="col-md-4 control-label">
                                                                Estado
                                                            </label>
                                                            <div class="col-md-8">
                                                                <select id="estado" class="form-control">
                                                                <option value="0">Selecciona un Estado</option>
                                                                <?php
                                                                foreach($rs_estados as $Estado){
                                                                    $selectEstado = $Estado["Id"]==$zona["Estado"]?"selected=selected":"";
                                                                    echo "<option value='".$Estado["Id"]."'".$selectEstado.">".$Estado["Estado"]."</option>";
                                                                }
                                                                ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="ciudades">
                                                            <label class="col-md-4 control-label">
                                                                Ciudad
                                                            </label>
                                                            <div class="col-md-8">
                                                                <select id="ciudad" class="form-control" onchange="GetHoteles(this);">
                                                                <?php
                                                                foreach($rs_ciudades as $ciudad){
                                                                    $selectCiudad = $ciudad["Id"]==$zona["Ciudad"]?"selected=selected":"";
                                                                    echo "<option value='".$ciudad["Id"]."'".$selectCiudad." data-destino='".$ciudad["Destino"]."' id='destinyOpt".$ciudad["Id"]."'>".$ciudad["Ciudad"]."</option>";
                                                                }
                                                                ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-6 checkbox checkbox-primary" style="height: 200px; overflow: auto;">
                                                    <ul id="listahoteles" style="list-style-type:none;">

                                                    </ul>
                                                </div>
                                                <div class="col-md-6" style="height: 200px; overflow: auto;">
                                                    <ul id="hotelesactivos" style="list-style-type:none;">
                                                        <?php foreach ($rs_hoteles as $hoteles) { ?>
                                                            <li id='element<?php echo $hoteles["Hotel"] ?>'>
                                                                <span class='hoteldropoff' id='<?php echo $hoteles["Hotel"] ?>'>
                                                                    <?php echo $hoteles["nombre_hotel"] ?>
                                                                </span>
                                                                <i class='zmdi zmdi-delete pull-right' id='deleter<?php echo $hoteles["Hotel"] ?>' data-hotel='<?php echo $hoteles["Hotel"] ?>' style='cursor:pointer' onclick='Erase(this);'></i>
                                                            </li>                                                        
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <button id="saveBtn" class="btn btn-primary">
                                               <i class="zmdi zmdi-floppy"></i> Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->                               
                </div> <!-- content -->
                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>
        </div>   
        <script type="text/javascript">
        $("#pais").change(function(){
            if($( "#pais" ).val() > 0){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaEstadosPorPais/'+$( "#pais" ).val(), 
                    type : 'GET',
                    success : function(data) {
                            console.log(data);
                            $( "#estados" ).html(data);                        
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
               $( "#estados" ).html("");  
               $( "#ciudades" ).html(""); 
            }
        });
        $("#estado").change(function(){
            console.log($( "#estado" ).val());
            if($( "#estado" ).val() > 0){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaCiudadesPorEstado/'+$( "#estado" ).val(), 
                    type : 'GET',
                    success : function(data) {
                        console.log(data);
                        $( "#ciudades" ).html(data);                        
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
                $( "#ciudades" ).html("");  
            }
        });

        function GetHoteles(destino){
            $( "#listahoteles" ).html("<div class='col-lg-12 center-block text-center'style='display:flex;justify-content:center;align-content:center;flex-direction:column;height:200px;'><p><b style='margin-top:-10px'>Obteniendo Hoteles </b><i class='fa fa-circle-o-notch fa-spin fa-fw'></i></p></div>");
            //console.log("Ejecuta");
            if($( destino ).val() > 0){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaHotelesPorDestino/'+$( "#destinyOpt"+$( destino ).val() ).data("destino")+'/<?php echo $zona['Id'] ?>', 
                    type : 'GET',
                    success : function(data) {
                            //console.log(data);
                            $( "#listahoteles" ).html("");
                            $( "#listahoteles" ).html(data);                        
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
               $( "#listahoteles" ).html("");
            }
        };        
        function CargaHoteles(destino){
            $( "#listahoteles" ).html("<div class='col-lg-12 center-block text-center'style='display:flex;justify-content:center;align-content:center;flex-direction:column;height:200px;'><p><b style='margin-top:-10px'>Obteniendo Hoteles </b><i class='fa fa-circle-o-notch fa-spin fa-fw'></i></p></div>");
            //console.log("Ejecuta");
            $.ajax({
                url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaHotelesPorDestino/'+destino+'/<?php echo $zona['Id'] ?>', 
                type : 'GET',
                success : function(data) {
                    //console.log(data);
                    $( "#listahoteles" ).html("");
                    $( "#listahoteles" ).html(data);                        
                },
                error : function(xhr, status) {
                    swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
                },
                complete : function(xhr, status) {
                }
            });
        };
        $( "#saveBtn" ).click(function() {
            if($( "#ciudad" ).val() != null){
                console.log($( "#ciudad" ).val());
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarZona/<?php echo $zona["Id"]; ?>', 
                    data : {
                        nombre : $( "#nombreZona" ).val(),
                        ciudad : $( "#ciudad" ).val()
                    }, 
                    type : 'POST',
                    success : function(data) {
                            $( ".hoteldropoff" ).each(function(){
                                $.ajax({
                                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarHotelAZona', 
                                    data : {
                                        hotel : $(this).attr("id"),
                                        zona : <?php echo $zona["Id"]; ?>
                                    }, 
                                    type : 'POST',
                                    success : function(data) {
                                            
                                    },
                                    error : function(xhr, status) {
                                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                                    },
                                    complete : function(xhr, status) {
                                    }
                                });
                            });
                            //console.log(data);
                            swal({   
                                title: "Guardado!",   
                                text: "Los cambios fueron correctamente Guardados ¿Desea continuar en esta pagina o regresar al listado general?",   
                                type: "success",   
                                showCancelButton: true,   
                                confirmButtonText: "Ir al Listado",   
                                cancelButtonText: "Permanecer",   
                                closeOnConfirm: false,   
                                closeOnCancel: true 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/Ciudades');
                                } else {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarCiudad/'+data.replace(" ","")); 
                                } 
                            });
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
                swal("Error", "Por favor, seleccione un Pais, Estado y Ciudad.", "error"); 
            }                        
        });
        <?php
        foreach($rs_ciudades as $ciudadZ){
            if($ciudadZ["Id"] == $zona["Ciudad"]){
        ?>
                $( document ).ready(function() {
                    CargaHoteles(<?php echo $ciudadZ["Destino"] ?>);
                });
        <?php
            }
        }
        ?>        
        </script>