<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->
            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Agregar Tipo Traslado</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Maravilla Maya</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Tipo Traslado</a></li>
                                    <li class="active">Nuevo Tipo Traslado</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Nuevo Tipo Traslado</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-horizontal col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Nombre del tipo de Traslado
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="tipoTraslado" placeholder="Nombre del tipo de traslado">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Descripción
                                                        </label>
                                                        <div class="col-md-8">
                                                            <textarea class="form-control" id="descripcion" rows="5"></textarea>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="m-b-30">
                                                            <button id="addToTable" class="btn btn-primary waves-effect waves-light">Agregar <i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <table class="table table-bordered table-striped" id="datatable-editable">
                                                    <thead>
                                                        <tr>
                                                            <th class="col-md-8">Inclusion</th>
                                                            <th class="col-md-1">Activo</th>
                                                            <th  class="col-md-1">Costo</th>
                                                            <th  class="col-md-2">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <button id="saveBtn" class="btn btn-primary">
                                               <i class="zmdi zmdi-floppy"></i> Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->                               
                </div> <!-- content -->
                <footer class="footer text-right">
                    2016 © Go Guy Travel.
                </footer>
            </div>
        </div>   
        <script type="text/javascript">        
        $( "#saveBtn" ).click(function() {
            if($( "#tipoTraslado" ).val() != null){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarTipoTraslado', 
                    data : {
                        nombre : $( "#tipoTraslado" ).val(),
                        descripcion : $( "#descripcion" ).val()
                    }, 
                    type : 'POST',
                    success : function(data) {
                            $('tbody > [role=row]').each(function(){
                                console.log($(this).children('.sorting_1').text());
                                console.log($(this).children('.chekbx2').text());
                                console.log($(this).children('.chekbx1').text());
                                $.ajax({
                                    url :'<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarInclusion/'+data.replace(" ",""), 
                                    data : {
                                        nombre  : $(this).children('.sorting_1').text(),
                                        activo  : $(this).children('.chekbx2').text()=="Si"?1:0,
                                        costo   : $(this).children('.chekbx1').text()=="Si"?1:0
                                    }, 
                                    type : 'POST',
                                    success : function(data) {
                                    },
                                    error : function(xhr, status) {
                                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error");
                                    },
                                    complete : function(xhr, status) {
                                    }
                                });
                            });
                            //console.log(data);
                            swal({   
                                title: "Guardado!",   
                                text: "Los cambios fueron correctamente Guardados ¿Desea continuar en esta pagina o regresar al listado general?",   
                                type: "success",   
                                showCancelButton: true,   
                                confirmButtonText: "Ir al Listado",   
                                cancelButtonText: "Permanecer",   
                                closeOnConfirm: false,   
                                closeOnCancel: true 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/TipoTraslado');
                                } else {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarTipoTraslado/'+data.replace(" ","")); 
                                } 
                            });
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
                swal("Error", "Por favor, Introduzca un nombre para este tipo de traslado.", "error"); 
            }                        
        });   
        $(document).on('change', '.chkbox', function(){
            if($(this).is(':checked')){
                $(this).val("Si");
            }else{
                $(this).val("No");
            }
        });   
        </script>