<?php if($rs_count > 0){ ?>
<label class="col-md-4 control-label">
	Estado
</label>
<div class="col-md-8">
	<select id="estado" class="form-control">
	<option value="0">Selecciona un Estado</option>
	<?php
	foreach($rs_estados as $row){
		echo "<option value='".$row["Id"]."'>".$row["Estado"]."</option>";
	}
	?>
	</select>
</div>
<script type="text/javascript">
	$("#estado").change(function(){
        console.log($( "#estado" ).val());
        if($( "#estado" ).val() > 0){
	        $.ajax({
	        	url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/ListaCiudadesPorEstado/'+$( "#estado" ).val(), 
	        	type : 'GET',
	        	success : function(data) {
	            	console.log(data);
	            	$( "#ciudades" ).html(data);                        
	        	},
	        	error : function(xhr, status) {
	            	swal("Error", "Ocurrio un error recuperando los datos, intente de nuevo por favor.", "error"); 
	        	},
	        	complete : function(xhr, status) {
	        	}
	        });
	    }else{
	    	$( "#ciudades" ).html("");  
	    }
    });
</script>
<?php }else{ ?>
<p class="col-md-4 control-label">
	Error
</p>
<div class="col-md-8">
<b>No hay Estados registrados para este pais, para continuar por favor agregue estados.</b>
</div>
<?php } ?>