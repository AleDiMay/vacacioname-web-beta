<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>
<?php foreach ($rs_estado as $row) { ?>
<?php } ?>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Editar Estado</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Estados</a></li>
                                    <li class="active"><?php echo $row['Estado'];?></li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><?php echo $row['Estado'];?></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-horizontal col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Nombre del Estado
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="nombreEstado" value="<?php echo $row['Estado'];?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Pais
                                                        </label>
                                                        <div class="col-md-8">
                                                            <select class="form-control" id="pais">
                                                            <?php foreach ($rs as $rowP){ ?>

                                                                <?php 
                                                                    $selected = ($rowP["Id"] == $row["Pais"])?"selected=selected":null;
                                                                ?>

                                                                <option <?php echo $selected;?> value='<?php echo $rowP["Id"];?>'>
                                                                    <?php echo $rowP["Pais"];?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <button id="saveBtn" class="btn btn-primary">
                                               <i class="zmdi zmdi-floppy"></i> Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->                               
                </div> <!-- content -->
                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>
        </div>   
        
        <script type="text/javascript">
        $( "#saveBtn" ).click(function() {
            $.ajax({
                url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarEstado/<?php echo $row['Id'];?>', 
                data : { 
                    nombre : $( "#nombreEstado" ).val(),
                    pais: $( "#pais" ).val()
                }, 
                type : 'POST',
                success : function(data) {
                        //console.log(data);
                        swal({   
                            title: "Guardado!",   
                            text: "Los cambios fueron correctamente Guardados ¿Desea continuar en esta pagina o regresar al listado general?",   
                            type: "success",   
                            showCancelButton: true,   
                            confirmButtonText: "Ir al Listado",   
                            cancelButtonText: "Permanecer",   
                            closeOnConfirm: false,   
                            closeOnCancel: true 
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/Estados');
                            } else {     
                                location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarEstado/'+data.replace(" ","")); 
                            } 
                        });
                },
                error : function(xhr, status) {
                    swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                },
                complete : function(xhr, status) {

                }
            });
        });
        </script>