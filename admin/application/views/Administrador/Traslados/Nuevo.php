<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->
            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Agregar Traslado</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Go Guy Travel</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li class="active">Agregar Nuevo</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Agregar Traslado</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-horizontal col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Nombre
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="traslado" placeholder="Nombre del Traslado">
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Zona
                                                        </label>
                                                        <div class="col-md-8">
                                                            <select class="select2" id="zona" data-placeholder="Selecciona una zona">
                                                                <option value="#" selected="selected">Selecciona una zona</option>
                                                                <?php foreach ($rs_zonas as $zonas) { ?>
                                                                    <option value="<?php echo $zonas["Id"] ?>"><?php echo $zonas["Nombre_Zona"] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">
                                                            Tipo de Traslado
                                                        </label>
                                                        <div class="col-md-8">
                                                            <select class="select2" id="tipo" data-placeholder="Selecciona un tipo de traslado">
                                                                <option value="#" selected="selected">Selecciona un tipo de traslado</option>
                                                                <?php foreach ($rs_tipotraslado as $tipo) { ?>
                                                                    <option value="<?php echo $tipo["Id"] ?>"><?php echo $tipo["Nombre"] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>                                                        
                                                    </div>                                              
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="m-b-30">
                                                            <button id="addToTable" class="btn btn-primary waves-effect waves-light">Agregar Precio <i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <table class="table table-bordered table-striped" id="datatable-editable">
                                                    <thead>
                                                        <tr>
                                                            <th class="col-md-6">Descripción</th>
                                                            <th class="col-md-2">Precio</th>
                                                            <th  class="col-md-2">Tipo Cambio</th>
                                                            <th  class="col-md-2">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                   </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <button id="saveBtn" class="btn btn-primary">
                                                <i class="zmdi zmdi-floppy"></i> Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->                               
                </div> <!-- content -->
                <footer class="footer text-right">
                    2015 © Maravilla Maya.
                </footer>
            </div>
        </div>   
        <script type="text/javascript">        
        $( "#saveBtn" ).click(function() {
            if($( "#traslado" ).val() != null){
                $.ajax({
                    url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/Agregar', 
                    data : {
                        nombre : $( "#traslado" ).val(),
                        zona : $( "#zona" ).val()
                    }, 
                    type : 'POST',
                    success : function(data) {
                            $('tbody > [role=row]').each(function(){
                                $.ajax({
                                    url :'<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarPrecioATraslado/', 
                                    data : {
                                        traslado: data.replace(" ",""),
                                        tipo: $("#tipo").val(),
                                        pax  : $(this).children('.descripr').data('pax'),
                                        precio  : $(this).children('.prelastr').text().replace("$",""),
                                        cambio   : $(this).children('.lastr').data('currency')
                                    }, 
                                    type : 'POST',
                                    success : function(data) {
                                    },
                                    error : function(xhr, status) {
                                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                                    },
                                    complete : function(xhr, status) {
                                    }
                                });
                            });
                            swal({   
                                title: "Guardado!",   
                                text: "Los cambios fueron correctamente Guardados ¿Desea continuar en esta pagina o regresar al listado general?",   
                                type: "success",   
                                showCancelButton: true,   
                                confirmButtonText: "Ir al Listado",   
                                cancelButtonText: "Permanecer",   
                                closeOnConfirm: false,   
                                closeOnCancel: true 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/Index');
                                } else {     
                                    location.replace('<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/'+data.replace(" ","")); 
                                } 
                            });
                    },
                    error : function(xhr, status) {
                        swal("Error", "Ocurrio un error con el guardado, intente de nuevo por favor.", "error"); 
                    },
                    complete : function(xhr, status) {
                    }
                });
            }else{
                swal("Error", "Por favor, Introduzca un nombre para este traslado.", "error"); 
            }                        
        });
        jQuery(".select2").select2({
            width: '100%'
        });
        </script>