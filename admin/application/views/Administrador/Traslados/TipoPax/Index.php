    <script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content --> 
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Catalogo de Tipos de Traslado</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Tipos de Traslado</a></li>
                                    <li class="active">Todos los Tipos de Traslado</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title col-md-6">Tipos de Traslado</h3>
                                        <div class="col-md-6">
                                            <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarTipoPax" class="btn btn-primary pull-right waves-effect waves-light">Agregar Tipo de Pax <i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                           
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Tipo de Traslado</th>
                                                            <th>Pasajeros</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tb
                                                    ody>
                                                    <?php foreach ($rs_tipopax as $rs_tipopax){ ?>
                                                        <tr>
                                                            <td class="col-md-7">
                                                                <?php echo $rs_tipopax["Nombre"]; ?>                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <?php echo $rs_tipopax["Minimo_Pax"]; ?> a <?php echo $rs_tipopax["Maximo_Pax"]; ?>                
                                                            </td>
                                                           <td class="col-md-3">
                                                                <div class="btn-group" role="group">
                                                                  <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/EditarTipoPax/<?php echo $rs_tipopax["Id"]; ?>" type="button" class="btn btn-primary btn-small"><i class="zmdi zmdi-edit"></i> Editar</a>
                                                                  <button type="button" class="btn btn-danger"><i class="zmdi zmdi-delete"></i> Eliminar</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>
        </div>