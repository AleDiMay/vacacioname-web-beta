	<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Traslados</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Moltran</a></li>
                                    <li><a href="#">Tables</a></li>
                                    <li class="active">Traslados</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Traslados</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Divisa</th>
                                                            <th>Codigo</th>
                                                            <th>Activo</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>

                                             
                                                    <tbody>
                                                    <?php foreach ($rs_currency as $row){ ?>
                                                        <tr>
                                                            <td class="col-md-5">
                                                                <?php echo $row["Nombre_Divisa"]; ?>                
                                                            </td>
                                                            <td class="col-md-1">
                                                                <?php echo $row["Codigo_Divisa"]; ?>
                                                            </td>
                                                            <td class="col-md-2">
                                                                <input type="checkbox" id="check<?php echo $row["Id"]; ?>" name="my-checkbox">
                                                            </td>
                                                            <td class="col-md-3">
                                                                <div class="btn-group" role="group">
                                                                  <a href="Traslados/Editar/<?php echo $row["Id"]; ?>" type="button" class="btn btn-primary btn-small"><i class="zmdi zmdi-edit"></i> Editar</a>
                                                                  <button type="button" class="btn btn-danger"><i class="zmdi zmdi-delete"></i> Eliminar</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <script>
                                                        $("[id='check<?php echo $row["Id"] ?>']").bootstrapSwitch({
                                                                <?php
                                                                    if($row["Activo"]==1){echo "state: true,";}
                                                                    else{echo "state: false,";}
                                                                ?>                 
                                                                size: "small",
                                                                onText: "Activo",
                                                                offText: "Inactivo",
                                                                onColor: "success",
                                                                offColor: "danger",
                                                                labelText: "Estado",
                                                                onSwitchChange(event, state){
                                                                    console.log(state);
                                                                    if(state){
                                                                        var estatus = 1;
                                                                    }else{
                                                                        var estatus = 0;
                                                                    }

                                                                    $.ajax({
                                                                        url : '<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/TipoCambio', 
                                                                        data : {
                                                                            Id : <?php echo $row["Id"] ?>,
                                                                            Estado : estatus
                                                                        }, 
                                                                        type : 'POST',
                                                                        success : function(data) {
                                                                            Materialize.toast('Cambios Guardados Correctamente', 4000);
                                                                        },
                                                                        error : function(xhr, status) {
                                                                            Materialize.toast('Ocurrio un error guardando la información', 4000);
                                                                        },
                                                                        complete : function(xhr, status) {
                                                                            
                                                                        }
                                                                    });
                                                                }
                                                            });

                                                        </script>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div> <!-- End Row -->


                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>
        </div>     