	<script src="http://cdn.boriz00piedrvanqlkjb51gwt1i7rebc9jtbxbyzt.netdna-cdn.com/admin/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js"></script>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
			<?php
                $this->load->helper('topbar'); 
            ?><!-- Top Bar End -->


            <!-- Left Sidebar Start -->
            <?php
                $this->load->helper('right-sidebar'); 
            ?>
            <div class="content-page">
                <!-- Start content --> 
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Catalogo de Ciudades</h4>
                                <ol class="breadcrumb pull-right">
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">Traslados</a></li>
                                    <li><a href="#">Ciudades</a></li>
                                    <li class="active">Todas los Ciudades</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title col-md-6">Ciudades</h3>
                                        <div class="col-md-6">
                                            <a href="<?php echo $GLOBALS['pathgeneral']; ?>admin/index.php/Administrador/Traslados/AgregarCiudad" class="btn btn-primary pull-right waves-effect waves-light">Agregar Ciudad <i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">                                        	
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Ciudad</th>
                                                            <th>Estado</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tb
                                                    ody>
                                                    <?php foreach ($rs_ciudades as $row){ ?>
                                                        <tr>
                                                            <td class="col-md-7">
                                                                <?php echo $row["Ciudad"]; ?>                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <?php
                                                                    foreach ($rs_estados as $rowE){
                                                                        if($row["Estado"] == $rowE["Id"]){
                                                                            echo $rowE["Estado"];
                                                                        }
                                                                    }
                                                                ?>                
                                                            </td>
                                                            <td class="col-md-3">
                                                                <div class="btn-group" role="group">
                                                                  <a href="EditarCiudad/<?php echo $row["Id"]; ?>" type="button" class="btn btn-primary btn-small"><i class="zmdi zmdi-edit"></i> Editar</a>
                                                                  <button type="button" class="btn btn-danger"><i class="zmdi zmdi-delete"></i> Eliminar</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer text-right">
                    2015 © Maravilla Maya.
                </footer>

            </div>
        </div>