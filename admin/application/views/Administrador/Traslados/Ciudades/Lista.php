<?php if($rs_count > 0){ ?>
<label class="col-md-4 control-label">
	Ciudad
</label>
<div class="col-md-8">
	<select id="ciudad" class="form-control" onchange="GetHoteles(this);">
		<option value="0">Selecciona una ciudad</option>
	<?php
	foreach($rs_ciudades as $row){
		echo "<option id='destinyOpt".$row["Id"]."' value='".$row["Id"]."' data-destino='".$row["Destino"]."'>".$row["Ciudad"]."</option>";
	}
	?>
	</select>
</div>
<?php }else{ ?>
<p class="col-md-4 control-label">
	Error
</p>
<div class="col-md-8">
<b>No hay Ciudades registradas para este estado, para continuar por favor agregue ciudades.</b>
</div>
<?php } ?>