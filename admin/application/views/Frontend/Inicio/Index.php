    <div class="top-shadow"></div>
        <div>
            <div class="search">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Trasalados</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Tours</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" style="padding: 10px;">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <form>
                                <div class="">
                                    <div class="form-group">
                                        <label for="destino">Destino</label>
                                        <select class="form-control" id="destino">
                                            <option selected="selected" value="#">Seleccione un Destino</option>
                                        <?php
                                            foreach ($rs_ciudades as $destino) {
                                                echo "<option value='".$destino['Id']."'>".$destino['Ciudad']."</option>";
                                            }
                                        ?>                          
                                        </select>
                                    </div> 
                                    <div class="form-group">
                                        <label for="fechas">Fecha(s)</label>
                                        <input type="text" name="fechas" readonly="readonly" class="form-control col-md-12 dates" data-range="true" data-multiple-dates-separator=" - " data-language="en" placeholder="Fechas">
                                    </div>
                                    <div class="form-group">
                                        <label for="destino">Destino</label>
                                        <select class="form-control" id="destino">
                                            <option selected="selected" value="#">Seleccione un Hotel</option>
                                        <?php
                                            foreach ($rs_ciudades as $destino) {
                                                echo "<option value='".$destino['Id']."'>".$destino['Ciudad']."</option>";
                                            }
                                        ?>                          
                                        </select>
                                    </div>                    
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="owl-example" class="owl-carousel owl-theme">  
            <div class="item"><img src="http://www.maravillamaya.com/assets/Frontend/img/home/main.jpg" alt="Cancun shuttle"></div>
            <div class="item"><img src="http://www.maravillamaya.com/assets/Frontend/img/xcaret.jpg" alt="Xcaret"></div>
            <div class="item"><img src="http://www.maravillamaya.com/assets/Frontend/img/xenotes.jpg" alt="Xenotes Cancun"></div>
        </div>
    <div class="container">
        <div class="row about-row hidden-xs">
            <div class="col-md-4 text-center">
                <center class="hidden-xs"><img src="http://www.maravillamaya.com/assets/Frontend/img/home/icon1.png" class="img-responsive"></center>
                <h3>Servicio VIP</h3>
                <p class="tight">Recibe un servicio de alta calidad a un precio bajo.</p>
            </div>
            <div class="col-md-4 text-center">
                <center class="hidden-xs"><img src="http://www.maravillamaya.com/assets/Frontend/img/home/icon2.png" class="img-responsive"></center>
                <h3>Vehiculos de calidad</h3>
                <p class="tight">Traslados siempre con vehiculos de ultimo modelo para tu confort.</p>
            </div>
            <div class="col-md-4 text-center">
                <center class="hidden-xs"><img src="http://www.maravillamaya.com/assets/Frontend/img/home/icon3.png" class="img-responsive"></center>
                <h3>Precios competitivos</h3>
                <p class="tight">Aqui tenemos los mejores precios del mercado. </p>
            </div>
        </div>
        <div class="row about-row visible-xs">
            <div class="col-xs-4">
                <img src="http://www.maravillamaya.com/assets/Frontend/img/home/icon1.png" class="img-responsive">
            </div>
            <div class="col-xs-8">
                <h3>Servicio VIP</h3>
                <p>Recibe un servicio de alta calidad a un precio bajo.</p>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-4">
                <img src="http://www.maravillamaya.com/assets/Frontend/img/home/icon2.png" class="img-responsive">
            </div>
            <div class="col-xs-8">
                <h3>Vehiculos de calidad</h3>
                <p>Traslados siempre con vehiculos de ultimo modelo para tu confort.</p>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-4">
                <img src="http://www.maravillamaya.com/assets/Frontend/img/home/icon3.png" class="img-responsive">
            </div>
            <div class="col-xs-8">
                <h3>Precios competitivos</h3>
                <p>Aqui tenemos los mejores precios del mercado. </p>
            </div>
        </div>
    </div>
    <div id="home-bkgd">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>¡Podemos recogerlo y/o dejarlo en aeropuerto de Cancun!</h3>
                    <h4>Escoga su destino para ver nuestros precios...</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="row links">
                        <div class="col-md-6">
                            <ul>
                                <li><a href="/traslados">Zona Hotelera Cancún</a></li>
                                <li><a href="/traslados">Centro Cancún</a></li>
                                <li><a href="/traslados">Puerto Juarez</a></li>
                                <li><a href="/traslados">Playa del Carmen</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li><a href="/traslados">Cozumel</a></li>
                                <li><a href="/traslados">Puerto Aventuras</a></li>
                                <li><a href="/traslados">Tulum</a></li>
                                <li><a href="/traslados">Chichen Itza</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <p>¿No ve su destino? No se preocupe, contáctanos para su oferta personalizada hoy. </p>
                    <a href="contacto" class="btn btn-primary btn-lg btn-block">Contáctanos ahora</a>
                </div>
            </div>
        </div>
    </div>