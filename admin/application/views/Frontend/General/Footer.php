    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Copyright &copy; <?= date('Y') ?> - Maravilla Maya Global Tours</p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="http://www.maravillamaya.com/assets/Frontend/owl-carousel/js/owl.carousel.min.js"></script>
    <script src="http://www.maravillamaya.com/assets/Frontend/js/select2.js"></script>
    <script src="http://www.maravillamaya.com/assets/Frontend/js/datepicker.js"></script>
    <script src="http://t1m0n.name/air-datepicker/dist/js/i18n/datepicker.en.js"></script>
    <script>
      $(document).ready(function() {
        $("#owl-example").owlCarousel({
          navigation : true, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem: true,
          pagination: false,
          autoPlay: true,
          navigationText: ['<i class="fa fa-chevron-left fa-2x"></i>','<i class="fa fa-chevron-right fa-2x"></i>']
        });
        $('select').select2({
          placeholder: 'Select an option'
        });
        $('.dates').datepicker({
          minDate: new Date() // Now can select only dates, which goes after today
        });
      });
    </script>
  </body>
</html>