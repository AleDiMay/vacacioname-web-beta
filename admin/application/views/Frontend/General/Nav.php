<nav class="navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index"><img src="http://www.maravillamaya.com/assets/Frontend/img/logo.png" class="img-responsive"></a>
        </div>

        <div class="collapse navbar-collapse navbar-right">
            <div class="phone"><span>Reserva tu traslado hoy: +52 998 138 8087</span></div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">Inicio</a></li>
                <li><a href="quienes-somos">Quienes Somos</a></li>
                <li><a href="traslados">Traslados</a></li>
                <li><a href="top-tours">Tours</a></li>
                <li><a href="reservaciones">Reservas</a></li>
                <li><a href="contacto">Contacto</a></li>
            </ul>
        </div>
        <!--.nav-collapse -->
    </div>
</nav>