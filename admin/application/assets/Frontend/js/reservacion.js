$(document).ready(function () {
    $('#characterLeft').text('Quedan 300 caracteres');
    $('#message').keydown(function () {
        var max = 300;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('Haz llegado al limite de caracteres');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');
        } else {
            var ch = max - len;
            $('#characterLeft').text('Quedan ' + ch + ' caracteres en este mensaje');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');
        }
    });



    $("#restype").on('change', function (e) {
        e.preventDefault();
        if ($("#restype").val() == 1) {
            $("#tours").show();
            $("#trans").hide();
            $("#choosenow").html('');
        } else if ($("#restype").val() == 2) {
            $("#tours").hide();
            $("#trans").show();
            $("#choosenow").html('');
        } else {
            $("#choosenow").html('<div class="alert alert-danger">Favor de seleccionar un tipo de servicio para reservar.</div>');
            $("#tours").hide();
            $("#trans").hide();
        }
    });

    $("#ser").on('change', function (e) {
        e.preventDefault();
        if ($("#ser").val() == 1 || $("#ser").val() == 2) {
            $("#date2").hide();
            $("#date1 label").html('Fecha de traslado');
        } else {
            $("#date2").show();
            $("#date1 label").html('Llegando');
        }
    });


    $("#submit").on('click', function (e) {
        e.preventDefault();
        // 'this' refers to the current submitted form
        var str = $('#res-form').serialize();
        var validSer = $("restype").val();

        if (validSer != 0) {
            $.ajax({
                type: "POST",
                url: "/dist/inc/process-reservation.php",
                data: str,

                success: function (msg) {
                    console.log('success');
                    // Message Sent? Show the 'Thank You' message and hide the form
                    if (msg == 'OK') {
                        result = '<div class="alert alert-success"><i class="fa fa-check"></i> Su reserva ha sido enviado con exito, estaremos respondiendo con su confirmación lo mas pronto posible.</div>';
                        $("#name").val('');
                        $("#email").val('');
                        $("#phone").val('');
                        $("#subject").val('');
                        $("#message").val('');
                        $("#tours").hide();
                        $("#trans").hide();
                        console.log('message ok');
                    } else {
                        result = msg;
                        console.log('message not ok ' + result);
                    }

                    $("#note").html(result);
                }
            });
        } else {
            $("#note").html('<div class="alert alert-danger">Favor de seleccionar un tipo de servicio para su reserva.</div>');
        }
    });

});
