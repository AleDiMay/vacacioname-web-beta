$(document).ready(function () {
    $('#characterLeft').text('Quedan 300 caracteres');
    $('#message').keydown(function () {
        var max = 300;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('Haz llegado al limite de caracteres');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');
        } else {
            var ch = max - len;
            $('#characterLeft').text('Quedan ' + ch + ' caracteres en este mensaje');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');
        }
    });
    console.log('document is ready!');
    $("#submit").on('click', function (e) {
        e.preventDefault();
        // 'this' refers to the current submitted form
        var str = $('#contact-form').serialize();
        console.log('submitted');

        $.ajax({
            type: "POST",
            url: "/dist/inc/process-contact.php",
            data: str,

            success: function (msg) {
                console.log('success');
                // Message Sent? Show the 'Thank You' message and hide the form
                if (msg == 'OK') {
                    result = '<div class="alert alert-success"><i class="fa fa-check"></i> Su mensaje ha sido enviado con exito, estaremos respondiendo lo mas pronto posible.</div>';
                    $("#name").val('');
                    $("#email").val('');
                    $("#phone").val('');
                    $("#subject").val('');
                    $("#message").val('');
                    console.log('message ok');
                } else {
                    result = msg;
                    console.log('message not ok ' + result);
                }

                $("#note").html(result);
            }
        });
        console.log('WTF');
    });
});
