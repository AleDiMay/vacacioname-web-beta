<?php
if (!defined('BASEPATH'))
  	exit('No direct script access allowed');

class Booking extends CI_Controller {
	function __construct()
  	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	public function index() {
		redirect('/Inicio/Index');
	}
	public function Transfers(){
	$details = array('modulo' => 3, 'title' => 'Finish Your Booking');
	$service = $_POST["traslado"];
	$info = $this->session->userdata('info');
	$client = $this->session->userdata('client');
	$this->load->model('Traslado_model', '', TRUE);
	$this->load->model('Cliente_model', '', TRUE);
	$traslado = $this->Traslado_model->GetAllTransferInfo($service)->row_array();
	$inclusiones = $this->Traslado_model->GetInclusiones($traslado["Tipo_Traslado_Id"]);
	//Get the info of the citys
	$ciudad = $this->Traslado_model->GetCiudad($info["ciudad"])->row_array();	
	$aeropuerto = $this->Traslado_model->GetAirport($ciudad["Aeropuerto"])->row_array();
	$rs = array('info' => $info, 'traslado' => $traslado, 'ciudad' => $ciudad, 'inclusion' => $inclusiones->result_array(), 'aeropuerto' => $aeropuerto, 'config' => $details);
	$reserva = $this->Cliente_model->AddProductTransfer($client, $service, $traslado["Tipo_Traslado_Nombre"], $info["hotel"], 14, 0, $traslado["Precio"], $info["llegada"], $info["regreso"], $info["adultos"], $info["menores"]);
	$this->session->set_userdata('reservation_id', $reserva);

	$this->load->view('General/Head', $rs);
	$this->load->view('General/Nav', $rs);
	$this->load->view('Booking/Transfer', $rs);
	$this->load->view('General/Footer', $rs);
	$this->load->view('General/Scripts', $rs);
	}
	public function Search(){
		
	}
}