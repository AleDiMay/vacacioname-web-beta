<?php
if (!defined('BASEPATH'))
  	exit('No direct script access allowed');

class Confirmation extends CI_Controller {
	function __construct()
  	{
		parent::__construct();
	}
	public function index() {
		redirect('/Inicio/Index');
	}
	public function Transfers(){
	//Post DATA
    $nombre = $_POST["Name"];
    $apellido = $_POST["Lastname"];
    $email = $_POST["email"];
    $telefono = $_POST["phone"];
    $aerolinea_llegada = $_POST["arrival_airline"];
    $aerolinea_regreso = $_POST["departure_airline"];
    $vuelo_llegada = $_POST["arrival_flight"];
    $vuelo_regreso = $_POST["departure_flight"];
    //Load Models
	$this->load->model('Traslado_model', '', TRUE);
	$this->load->model('Cliente_model', '', TRUE);
	//Declare Module info	
	$details = array('modulo' => 4, 'title' => 'Thank You!');
	//Get the session vars
	$info = $this->session->userdata('info');
	$client = $this->session->userdata('client');
	$reservation_id = $this->session->userdata('reservation_id');
	//Get the info form database
	$reserva = $this->Cliente_model->GetReservaTraslado($reservation_id)->row_array();
	$traslado = $this->Traslado_model->GetAllTransferInfo($reserva["Id_Servicio"])->row_array();
	$inclusiones = $this->Traslado_model->GetInclusiones($reserva["Id_Servicio"]);
	$ciudad = $this->Traslado_model->GetCiudad($info["ciudad"]);
	//Confirm the reservation
	$confirmation = $this->Cliente_model->ConfirmReservationTransfer($reservation_id, $aerolinea_llegada, $aerolinea_regreso, $vuelo_llegada, $vuelo_regreso, $info["pax"])->row_array();
	$clienteUpdate = $this->Cliente_model->UpdateClientData($client, $nombre, $apellido, $email, $telefono)->row_array();
	//Construct array for send parameters to the view
	$rs = array('info' => $info, 'traslado' => $traslado, 'ciudad' => $ciudad->row_array(), 'inclusion' => $inclusiones->result_array(), 'config' => $details, 'reserva' => $confirmation);
	redirect('/Confirmation/TransportationVoucher/'.$confirmation["Conf_Id"].'/'.$clienteUpdate["Email"]);
	}
	function TransportationVoucher($confirmation, $email_addres){
		$this->load->model('Cliente_model', '', TRUE);
		$this->load->model('Traslado_model', '', TRUE);
		$reserva = $this->Cliente_model->GetReservaByConfirmation($confirmation)->row_array();
		$cliente = $this->Cliente_model->GetClienteById($reserva["client"])->row_array();
		$traslado = $this->Traslado_model->GetAllTransferInfo($reserva["Id_Servicio"])->row_array();
		$zona = $this->Traslado_model->GetZona($traslado["Zona_Id"])->row_array();
		$ciudad = $this->Traslado_model->GetCiudad($zona["Ciudad"])->row_array();
		$aeropuerto = $this->Traslado_model->GetAirport($ciudad["Aeropuerto"])->row_array();
		if($cliente["Email"] == "$email_addres"){
			$details = array('modulo' => 4, 'title' => 'Thank You!');
			$rs = array('reserva' => $reserva, 'cliente' => $cliente, 'config' => $details, 'aeropuerto' => $aeropuerto, 'ciudad' => $ciudad);
			$this->load->view('General/Head', $rs);
			$this->load->view('General/Nav', $rs);
			$this->load->view('Confirmation/Transfer', $rs);
			$this->load->view('General/Footer', $rs);
			$this->load->view('General/Scripts', $rs);
		}
	}
	private function createFolder()
    {
        if(!is_dir("./files"))
        {
            mkdir("./files", 0777);
            mkdir("./files/pdfs", 0777);
        }
    }
	function PrintVoucher($confirmation, $email){
		$this->load->model('Cliente_model', '', TRUE);
		$this->load->model('Traslado_model', '', TRUE);
		$this->load->library('ciqrcode');
		$reserva = $this->Cliente_model->GetReservaByConfirmation($confirmation)->row_array();
		$cliente = $this->Cliente_model->GetClienteById($reserva["client"])->row_array();
		$traslado = $this->Traslado_model->GetAllTransferInfo($reserva["Id_Servicio"])->row_array();
		$zona = $this->Traslado_model->GetZona($traslado["Zona_Id"])->row_array();
		$ciudad = $this->Traslado_model->GetCiudad($zona["Ciudad"])->row_array();
		$aeropuerto = $this->Traslado_model->GetAirport($ciudad["Aeropuerto"])->row_array();
		if($cliente["Email"] == $email){

			$confirId = $reserva["Conf_Id"];
			$params['data'] = $confirId;
	 		$params['level'] = 'H';
	 		$params['size'] = 30;
	 		$params['savename'] = FCPATH.'qr_code/'.$confirId.'.png';
	 		$this->ciqrcode->generate($params);
	 		$data = array('reserva' => $reserva, 'cliente' => $cliente, 'traslado' => $traslado, 'zona' => $zona, 'ciudad' => $ciudad, 'aeropuerto' => $aeropuerto);
	 		$this->load->library('Html2pdf');
	 		
	 		$this->createFolder();
	 		$this->html2pdf->folder('./files/pdfs/');	        
	        //establecemos el nombre del archivo
	        $this->html2pdf->filename($confirId.'.pdf');	        
	        //establecemos el tipo de papel
	        $this->html2pdf->paper('a4', 'portrait');	 		
	 		$this->html2pdf->html($this->load->view('General/Voucher', $data, true));      
	        //si el pdf se guarda correctamente lo mostramos en pantalla
	        if($this->html2pdf->create('save')) 
	        {
	            $this->show();
	        }
		}
		
	}
	//funcion que ejecuta la descarga del pdf
    public function downloadPdf()
    {
        //si existe el directorio
        if(is_dir("./files/pdfs"))
        {
            //ruta completa al archivo
            $route = base_url("files/pdfs/test.pdf"); 
            //nombre del archivo
            $filename = "test.pdf"; 
            //si existe el archivo empezamos la descarga del pdf
            if(file_exists("./files/pdfs/".$filename))
            {
                header("Cache-Control: public"); 
                header("Content-Description: File Transfer"); 
                header('Content-disposition: attachment; filename='.basename($route)); 
                header("Content-Type: application/pdf"); 
                header("Content-Transfer-Encoding: binary"); 
                header('Content-Length: '. filesize($route)); 
                readfile($route);
            }
        }    
    }
 
 
    //esta función muestra el pdf en el navegador siempre que existan
    //tanto la carpeta como el archivo pdf
    public function show()
    {
        if(is_dir("./files/pdfs"))
        {
            $filename = "test.pdf"; 
            $route = base_url("files/pdfs/test.pdf"); 
            if(file_exists("./files/pdfs/".$filename))
            {
                header('Content-type: application/pdf'); 
                readfile($route);
            }
        }
    }
    
    //función para crear y enviar el pdf por email
    //ejemplo de la libreria sin modificar
    public function mail_pdf()
    {
        
        //establecemos la carpeta en la que queremos guardar los pdfs,
        //si no existen las creamos y damos permisos
        $this->createFolder();
 
        //importante el slash del final o no funcionará correctamente
        $this->html2pdf->folder('./files/pdfs/');
        
        //establecemos el nombre del archivo
        $this->html2pdf->filename('test.pdf');
        
        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');
        
        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'Listado de las provincias españolas en pdf',
            'provincias' => $this->pdf_model->getProvincias()
        );
        
        //hacemos que coja la vista como datos a imprimir
        //importante utf8_decode para mostrar bien las tildes, ñ y demás
        $this->html2pdf->html(utf8_decode($this->load->view('pdf', $data, true)));


        //Check that the PDF was created before we send it
        if($path = $this->html2pdf->create('save')) 
        {

            $this->load->library('email');
 
            $this->email->from('your@example.com', 'Your Name');
            $this->email->to('israel965@yahoo.es'); 
            
            $this->email->subject('Email PDF Test');
            $this->email->message('Testing the email a freshly created PDF');    
 
            $this->email->attach($path);
 
            $this->email->send();
            
            echo "El email ha sido enviado correctamente";
                        
        }
        
    }
}