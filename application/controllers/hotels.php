<?php
if (!defined('BASEPATH'))
  	exit('No direct script access allowed');

class Hotels extends CI_Controller {
	function __construct()
  	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	public function index() {
		$this->load->helper('xml');
    	//get the raw textdata of sample.xml
  		$xmlRaw = file_get_contents("http://testxml.e-tsw.com/AffiliateService/AffiliateService.svc/restful/GetQuoteHotels?a=vexperienceA&ip=201.147.113.232&c=pe&sd=20160801&ed=20160804&h=&rt=&mp=&r=1&r1a=1&r1k=0&r1k1a=0&r1k2a=0&r1k3a=0&r2a=0&r2k=0&r2k1a=0&r2k2a=0&r2k3a=0&r3a=0&r3k=0&r3k1a=0&r3k2a=0&r3k3a=0&r4a=0&r4k=0&r4k1a=0&r4k2a=0&r4k3a=0&r5a=0&r5k=0&r5k1a=0&r5k2a=0&r5k3a=0&d=2&l=esp&hash=hs:true");
  		//echo $xmlRaw;
		//load the simplexml library NOTE, this is a userdefined library @See libraries/simplexml.php
		$this->load->library('simplexml');
		
		//use the method to parse the data from xml
		$xmlData = $this->simplexml->xml_parse($xmlRaw);
		//set the data
		$data["xmlData"] = $xmlData;
		//load the view/xmlparcer.php along with the data
		$this->load->view('hotels', $data);
	}
}