<?php
if (!defined('BASEPATH'))
  	exit('No direct script access alloweds');

class Fares extends CI_Controller {
	function __construct()
  	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	public function index() {
		$this->load->view('General/Head.php');
		$this->load->view('General/Nav.php');
		$this->load->view('Inicio/Index.php');
		$this->load->view('General/Footer.php');
		$this->load->view('General/Scripts.php');
	}
	public function Testing(){
		echo $this->input->ip_address();
	}
	public function Transfers(){
		$this->load->model('Cliente_model', '', TRUE);
		$details = array('modulo' => 2, 'title' => 'Select The Best Service For You');
		//Construct the client session
		if($this->session->userdata('client') == null){
			$sessionid = random_string('alnum', 24);
			$clientip = $this->input->ip_address();
			$clientbrowser = $this->agent->browser();
			$clientplatform = $this->agent->platform();
			//Insert This Session into database
			$client_insert = $this->Cliente_model->Add($sessionid, $clientip, $clientbrowser, $clientplatform);
			$this->session->set_userdata('client', $client_insert);
			//echo $client["session_id"];			
		}
		//Destroy old session data
		$this->session->unset_userdata('info');		
		$this->session->unset_userdata('cityinfo');
		//Request Post�s data
		$zona = $_POST["zona"];
		$ciudad = $_POST["ciudad"];
		$adultos = $_POST["adultos"];
		$menores = $_POST["menores"];
		$llegada = date('Y-m-d', strtotime($_POST["date_from"]));
		$regreso = date('Y-m-d', strtotime($_POST["date_to"]));
		$hotel = $_POST["hotel"];
		$pax = $adultos + $menores;
		//Construct an array with all the general info
		$info = array('adultos' => $adultos, 'menores' => $menores, 'llegada' => $llegada, 'regreso' => $regreso, 'hotel' => $hotel, 'pax' => $pax, 'ciudad' => $ciudad, 'zona' => $zona, 'aeropuerto' => $_POST["city"]);
		//Load the Traslado Model
		$this->load->model('Traslado_model', '', TRUE);
		//Get the available services
		$traslados = $this->Traslado_model->GetTrasladoByPaxAndZona($pax, $zona);
		//Get the info of the city
		$ciudad = $this->Traslado_model->GetCiudad($ciudad)->row_array();
		//Get Airport Info
		$aeropuerto = $this->Traslado_model->GetAirport($ciudad["Aeropuerto"])->row_array();
		$aeropuertos = $this->Traslado_model->GetAirports()->result_array();
		//Get the inclusions
		$inclusiones = $this->Traslado_model->GetTodasInclusiones();
		//Construct the array with all the info
		$rs = array('rs_traslados' => $traslados->result_array(), 'info' => $info, 'rs_count' => $traslados->num_rows(), 'ciudad' => $ciudad, 'inclusiones' => $inclusiones->result_array(), 'aeropuerto' => $aeropuerto, 'aeropuertos' => $aeropuertos, 'config' => $details);		
		//Declare session vars
		$this->session->set_userdata('info', $info);
		//Load the views
		$this->load->view('General/Head', $rs);
		$this->load->view('General/Nav', $rs);
		$this->load->view('Fares/Transfer', $rs);
		$this->load->view('General/Footer', $rs);
		$this->load->view('General/Scripts', $rs);
	}
}