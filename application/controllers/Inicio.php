<?php
if (!defined('BASEPATH'))
  	exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()
  	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	public function index() {
		$this->session->sess_destroy();
		$this->load->model('Traslado_model', '', TRUE);
		$details = array('modulo' => 1, 'title' => 'Book Your Airport Transfer');
		$data = array('config' => $details, 'airports' => $this->Traslado_model->GetAirports()->result_array());		
		$this->load->view('General/Head', $data);
		$this->load->view('General/Nav', $data);
		$this->load->view('Inicio/Index', $data);
		$this->load->view('General/Footer', $data);
		$this->load->view('General/Scripts', $data);
	}
	public function ObtenerDestinos(){
		$busqueda = $_GET['term'];
		$this->load->model('Traslado_model', '', TRUE);
		$destinos = array('rs_destinos' => $this->Traslado_model->SearchCiudades($busqueda)->result_array());
		$this->load->view('General/Autocomplete', $destinos);
	}
	public function BuscarHoteles($destino){
		$busqueda = $_GET['term'];
		$this->load->model('Traslado_model', '', TRUE);
		$rs = array('rs' => $this->Traslado_model->SearchHotelesByDestino($destino, $busqueda)->result_array());
		$this->load->view('General/AutocompleteHoteles', $rs);
	}
	public function CotizaTraslado(){
		
	}
}