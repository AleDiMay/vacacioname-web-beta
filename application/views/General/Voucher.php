<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Voucher</title>
	<style type="text/css">
		body{
			
		}
		dt{
			font-weight: bold;
		}
		.main{
			width: 980px;
			padding: 23px 65px 65px;
			
		}
		.header{
			width: 100%;
		}
		.logo{
			width: 20%;
		}
		.title{			
			width: 60%;
			font-size: 25px;
			margin-left: 307px;
			margin-top: -48px;
		}
		.content{			
			display: inline-block;
		}
		.details{
			width: 510px;
			display: inline-block;
		}
		.code{
			width: 200px;
			float: right;
			display: inline-block;
		}
	</style>
</head>
<body>
	<div class="main">
		<div class="header">
			<img class="logo" src="http://goguytravel.com/assets/images/mdlogo-hor.png">
			<h1 class="title">Reservation: <?php echo $reserva["Conf_Id"] ?></h1>
			<hr>
		</div>
		<div class="content">
			<div class="details">
				<dt>Booking number:</dt><dd><?php echo $reserva["Conf_Id"] ?></dd>
	            <dt>First name:</dt><dd><?php echo $cliente["Nombre"] ?></dd>
	            <dt>Last name:</dt><dd><?php echo $cliente["Apellido"] ?></dd>
	            <dt>E-mail address:</dt><dd><?php echo $cliente["Email"] ?></dd>
            </div>
            <?php echo '<img class="code" src="'.base_url().'qr_code/'.$reserva["Conf_Id"].'.png" />'; ?>
		</div>
	</div>
</body>
</html>