<div id="page-wrapper">
<header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <li><a href="#">MY RESERVATION</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="main-header">                
                <div class="container">
                    <nav id="main-menu" role="navigation">
                        <h1 class="logo navbar-brand">
                            <a href="http://www.goguytravel.com/" title="Go Guy Travel">
                                <img class="img-responsive" src="http://goguytravel.com/assets/images/mdlogo-hor.png"/>
                            </a>
                        </h1>
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="/index.php">Transportation</a>                                
                            </li>
                        </ul>
                    </nav>
                </div>                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="/index.php">Transportation</a>
                        </li>
                    </ul>                    
                    <ul class="mobile-topnav container">
                        <li><a href="#">MY RESERVATION</a></li>                        
                    </ul>                    
                </nav>
            </div>
        </header>