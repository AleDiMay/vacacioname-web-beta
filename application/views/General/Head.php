<?php $assetsUrl = "http://goguytravel.com/assets/"; ?>
<!DOCTYPE html>
<head>
    <!-- Page Title -->
    <title>Go Guy Travel - <?php echo $config["title"]; ?></title>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/animate.min.css">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo $assetsUrl ?>components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo $assetsUrl ?>components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo $assetsUrl ?>components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo $assetsUrl ?>components/flexslider/flexslider.css" media="screen" />

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/smoothness/jquery-ui.css" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="<?php echo $assetsUrl ?>css/style.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/responsive.css">
    <link rel="stylesheet" href="<?php echo $assetsUrl ?>css/select2.css">
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <style type="text/css">
        .logo {width: 225px;}
    </style>    
</head>
<body>