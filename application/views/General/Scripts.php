<?php $assetsUrl = "http://goguytravel.com/assets/"; ?>
<!-- Javascript -->
    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/jquery-ui.1.10.4.min.js"></script>
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/bootstrap.js"></script>
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Flex Slider -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>components/flexslider/jquery.flexslider-min.js"></script>
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>components/jquery.bxslider/jquery.bxslider.min.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/jquery.stellar.min.js"></script>
    <!-- waypoint -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/waypoints.min.js"></script>
    <!-- load page Javascript -->
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/theme-scripts.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/scripts.js"></script>
    <script type="text/javascript" src="<?php echo $assetsUrl ?>js/select/select2.full.js"></script> 
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script> 
    <?php if($config["modulo"] == 1){ ?>
    <script type="text/javascript">
        tjq(".flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
        jQuery(document).ready(function($){

            var cityS;
            //you can now use $ as your jQuery object.
            var height = $( window ).height();
            var altoSlider = height-( $("header#header").height() + $("footer#footer").height() )-1;
            //console.log("Slider debe medir" + altoSlider - 5);
            $("#content").height(altoSlider);
            //Functions 
            $("#city").change(function(){   
                $("#hotel").val("");   
                $("#hotel").prop('disabled', false);
                console.log($(this).val());
                //Init the Autocomplete
                $("#hotel").autocomplete({
                    source: "http://goguytravel.com/Inicio/BuscarHoteles/"+$("#city").val()+"/",
                    minLength: 2,
                    select: function(event, ui) {
                        $("#HotelZone").val(ui.item.id);
                    }, 
                    html: true, // optional (jquery.ui.autocomplete.html.js required)
                    // optional (if other layers overlap autocomplete list)
                    open: function(event, ui) {
                        $(".ui-autocomplete").css("z-index", 1000);
                    }
                });
            });              
            $( window ).resize(function() {
                //you can now use $ as your jQuery object.
                var height = $( window ).height();
                var altoSlider = height-( $("header#header").height() + $("footer#footer").height() )-1;
                //console.log("Slider debe medir" + altoSlider - 5);
                $("#content").height(altoSlider);
            });
            $("#service").change(function(){
                if($(this).val() == 1){     
                    //Print Airport               
                    
                }
                if($(this).val() == 2){     
                    //Print Airport               
                    
                }
                if($(this).val() == 3){     
                    //Print Airport               
                    $("#placeholder").text("Choose Departure Airport");
                    console.log("Tipo 3");
                }
            });
            $("#bookertransfer").validate({
                rules: {
                    date_to: {
                        required: true,
                        date: true
                    },
                    date_from: {
                        required: true,
                        date: true
                    },
                    hotel:{
                        required: true,
                        minlength: 3
                    },
                    adultos: {
                        required: true,                        
                    },
                    city: "required",
                    adultos: "required"
                },
                messages: {
                    date_to: {
                        required: "Please Choose your Departure Date",
                        date: "Please Choose a Valid Date"
                    },
                    date_from: {
                        required: "Please Choose your Arrival Date",
                        date: "Please Choose a Valid Date"
                    },
                    hotel:{
                        required: "Please Choose your Hotel",
                        minlength: "Please Choose a Valid Hotel"
                    },
                    city: "Please Select your Airport",
                    adultos: "Please Select the number of adults"
                }
            });
        });
    </script>
    <?php } ?>
    <?php if($config["modulo"] == 3){ ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            //Functions 
            $("#clientinfo").validate({
                rules: {
                    Name: "required",
                    Lastname: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    verifyemail: {
                        required: true, 
                        equalTo: "[name='email']"                       
                    },
                    country: "required",
                    phone: {
                        required: true, 
                        phoneUS: true                       
                    },
                    arrival_airline: "required",
                    arrival_flight: "required",
                    departure_airline: "required",
                    departure_flight: "required",
                    payment: "required",
                    terms: "required"
                },
                messages: {
                    Name: "Enter a Valid Name",
                    Lastname: "Enter a Valid Lastname",
                    email: {
                        required: "Enter your E-mail Address",
                        email: "Enter a Valid Email Address"
                    },
                    verifyemail: {
                        required: "Verify your E-mail Address", 
                        equalTo: "This E-mail do not match whit E-mail Address input"                       
                    },
                    country: "Please Choose your Country",
                    phone: {
                        required: "Please Enter your Telephone Number", 
                        phoneUS: "Please Enter a Valid Telephone Number"                       
                    },
                    arrival_airline: "Please Provide your Arrival Airline",
                    arrival_flight: "Please Provide your Arrival Flight",
                    departure_airline: "Please Provide your Departure Airline",
                    departure_flight: "Please Provide your Departure Flight",
                    payment: "Choose your Payment Method",
                    terms: "For continue please acept our Terms & Conditions"
                }
            });
        });
    </script>
    <?php } ?>
    <?php if($config["modulo"] == 2){ ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            //Functions 
            $("#hotel").autocomplete({
                source: "http://goguytravel.com/Inicio/BuscarHoteles/1/",
                minLength: 2,
                select: function(event, ui) {
                    $("#HotelZone").val(ui.item.id);
                }, 
                html: true, // optional (jquery.ui.autocomplete.html.js required)
                // optional (if other layers overlap autocomplete list)
                open: function(event, ui) {
                    $(".ui-autocomplete").css("z-index", 1000);
                }
            });
        });
    </script>
    <?php } ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-81357979-1', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>