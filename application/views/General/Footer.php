        <footer id="footer">
            <div class="bottom">
                <div class="container">
                    <div class="logo pull-left">
                       <!--<a href="/" title="Go guy Travel - home">
                           <img src="/assets/images/mdlogo-hor.png" alt="Go Guy Travel" />
                        </a>-->
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; <?php echo date ("Y"); ?> Go Guy Travel</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>