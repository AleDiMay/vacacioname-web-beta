<style>

        section#content {  padding: 0; position: relative; overflow: hidden; }
        #main { padding-top: 30px; }
        .page-title, .page-description { color: #fff; }
        .page-title { font-size: 4.1667em; font-weight: bold; text-shadow: -2px -2px 1px #000, 2px 2px 1px #000, -2px 2px 1px #000, 2px -2px 1px #000; }
        .page-description { font-size: 21px; text-shadow: -2px -2px 1px #000, 2px 2px 1px #000, -2px 2px 1px #000, 2px -2px 1px #000; }
        .featured { position: absolute; right: 50px; top: 50px; z-index: 9; margin-bottom: 0;  text-align: right; }
        .featured figure a { border: 2px solid #fff; }
        .featured .details { margin-right: 10px; }
        .featured .details > * { color: #fff; line-height: 1.25em; margin: 0; font-weight: bold; text-shadow: 2px -2px rgba(0, 0, 0, 0.2); }        
        .ui-autocomplete{
            height: 100px;
            overflow: scroll;
        }
    </style>
<section id="content" class="slideshow-bg">
            <div id="slideshow">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="slidebg" style="background-image: url('http://consejosgratis.com/wp-content/uploads/2015/07/playa11.jpg');"></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <div id="main">
                    <h1 class="page-title">Let´s take you to your destination!</h1>
                    <h2 class="page-description col-md-6 no-float no-padding">Faster, Secure, Cheaper! Discover why we are your best option.</h2>
                    <div class="search-box-wrapper style2">
                        <div class="search-box">
                            <ul class="search-tabs clearfix">
                                <li class="active">
                                    <a href="#transfers" data-toggle="tab">
                                        <i class="soap-icon-pickanddrop"></i>
                                        <span>AIRPORT TRANSFERS</span>
                                    </a>
                                </li>

                                <li>

                                    <a href="#tours" data-toggle="tab">

                                        <i class="soap-icon-adventure"></i>

                                        <span>TOURS</span>

                                    </a>

                                </li>

                            </ul>

                            <div class="visible-mobile">

                                <ul id="mobile-search-tabs" class="search-tabs clearfix">

                                    <li class="active"><a href="#hotels-tab">AIRPORT TRANSFERS</a></li>

                                    <li><a href="#flights-tab">TOURS</a></li>

                                </ul>

                            </div>                            

                            <div class="search-tab-content">

                                <div class="tab-pane fade active in" id="transfers">

                                    <form id="bookertransfer" action="http://www.goguytravel.com/Fares/Transfers" method="post">
                                    <input type="hidden" name="ciudad" id="citySelected" value="2">
                                    <input type="hidden" id="HotelZone" name="zona" value="">

                                        <h4 class="title">Where do you want to go?</h4>

                                        <div class="row">
                                            <div class="form-group col-sm-6 col-md-1">
                                                <select id="service" class="form-control" name="service">
                                                    <option selected="selected" value="1">Round Trip</option>
                                                    <option value="2">Apto - Hotel</option>
                                                    <option value="3">Hotel - Apto</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-6 col-md-2" id="frow">
                                                <select id="city" name="city" class="form-control">
                                                    <option selected="selected" id="placeholder" value="">Choose Arrival Airport</option>
                                                    <?php
                                                        foreach ($airports as $airport) {
                                                            echo "<option value='".$airport['Id']."'>".$airport['Airport']." (".$airport["Iata"].")</option>";
                                                        }
                                                    ?>
                                                </select>
                                                
                                            </div>                                                
                                            <div class="form-group col-sm-6 col-md-2" id="srow">
                                                <input type="text" class="input-text form-control" name="hotel" id="hotel" disabled="disabled" placeholder="Choose a Hotel"/>
                                            </div>
                                            <div class="form-group col-sm-6 col-md-3">

                                                <div class="row">

                                                    <div class="col-xs-6">

                                                        <div class="datepicker-wrap">

                                                            <input type="text" name="date_from" id="date_from" class="input-text full-width" placeholder="Arrival"/>

                                                        </div>

                                                    </div>

                                                    <div class="col-xs-6">

                                                        <div class="datepicker-wrap">

                                                            <input type="text" name="date_to" class="input-text full-width" placeholder="Departure"/>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="form-group col-sm-12 col-md-4">

                                                <div class="row">

                                                    <div class="col-xs-4">
                                                        <select class="form-control" id="adultos" name="adultos">
                                                                <option selected="selected" value="">Adults</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                    </div>

                                                    <div class="col-xs-4">
                                                        <select selected="selected" class="form-control" name="menores">
                                                            <option selected="selected" value="">Childs</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-xs-4">
                                                        <button type="submit" class="full-width">SEARCH NOW</button>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </form>

                                </div>

                                <div class="tab-pane fade in" id="tours">

                                    <form action="hotel-list-view.html" method="post">

                                        <h4 class="title">Where do you want to go?</h4>

                                        <div class="row">

                                            <div class="form-group col-sm-6 col-md-10">

                                                <input type="text" class="input-text full-width" id="city" placeholder="Choose a Destination" />

                                            </div>

                                            <div class="form-group col-md-2">

                                                <div class="row">

                                                    <button type="submit" class="full-width">SEARCH NOW</button>

                                                </div>

                                            </div>

                                        </div>

                                    </form>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="featured image-box">

                <div class="details pull-left">

                    <h3>Luxury Transportation,<br/>Airport - Hotel</h3>

                    <h5>Cancun</h5>

                </div>

                <figure class="pull-left">

                    <a class="badge-container" href="#">

                        <span class="badge-content right-side">From $100</span>

                        <img width="64" height="64" alt="" src="https://www.colonylimo.com/images/gallery/suv/suv-colony-limo-colony-limo-service-houston-01.jpg" class="">

                    </a>

                </figure>

            </div>

        </section>