        <section id="content" class="gray-area">
            <div class="container">
                    <div id="main">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <h4 class="search-results-title"><i class="soap-icon-search"></i><b><?php echo $rs_count ?></b> results found.</h4>
                            <div class="toggle-container filters-container">
                                <div class="panel style1 arrow-right">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#modify-search-panel" class="collapsed">Modify Search</a>
                                    </h4>
                                    <div id="modify-search-panel" class="panel-collapse collapse in">
                                        <div class="panel-content">
                                            <form method="post" action="http://www.goguytravel.com/Fares/Transfers">
                                            <input type="hidden" name="ciudad" id="citySelected" value="2">
                                                <div class="form-group">
                                                    <label>Where we pick up you?</label>
                                                    <select class="form-control" name="city">
                                                        <?php foreach ($aeropuertos as $airp) {
                                                            echo "<option value='".$airp["Id"]."'>".$airp["Airport"]."</option>";
                                                        } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Where you want to go?</label>
                                                    <input type="text" class="form-control" name="hotel" id="hotel" placeholder="Choose a Hotel" value="<?php echo $info["hotel"] ?>" />
                                                    <input type="hidden" id="HotelZone" name="zona" value="<?php echo $info['zona'] ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>pick-up date</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" name="date_from" class="form-control" placeholder="mm/dd/yy" value="<?php echo $info['llegada'] ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>drop-off date</label>
                                                    <div class="datepicker-wrap">
                                                        <input type="text" name="date_to" class="form-control" placeholder="mm/dd/yy" value="<?php echo $info['regreso'] ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Adults</label>
                                                        <select name="adultos" class="form-control">
                                                            <?php
                                                                $limitA = 12;
                                                                $nA=1;
                                                                while($nA <= $limitA){
                                                                    if($nA == $info["adultos"]){
                                                                        echo "<option value='".$nA."' selected='selected'>".$nA."</option>";
                                                                    }else{
                                                                        echo "<option value='".$nA."'>".$nA."</option>";
                                                                    }
                                                                    $nA++;
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Children</label>
                                                        <select name="menores" class="form-control">
                                                            <?php
                                                                $limitC = 10;
                                                                $nC=0;
                                                                while($nC <= $limitC){
                                                                    if($nC == $info["menores"]){
                                                                        echo "<option value='".$nC."' selected='selected'>".$nC."</option>";
                                                                    }else{
                                                                        echo "<option value='".$nC."'>".$nC."</option>";
                                                                    }
                                                                    $nC++;
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br />
                                                <br />
                                                <button style="margin-top: 20px" type="submit" class="btn-medium icon-check uppercase full-width">search again</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div class="sort-by-section clearfix">
                               <h4 class="sort-by-title block-sm"><?php echo "From: ".$aeropuerto["Airport"]." (".$aeropuerto["Iata"].')  <i class="fa fa-arrows-h" aria-hidden="true"></i>  To: '.$info["hotel"]." (One Way Trip)"; ?></h4>
                                <!--<ul class="sort-bar clearfix block-sm">
                                    <li class="sort-by-name"><a class="sort-by-container" href="#"><span>name</span></a></li>
                                    <li class="sort-by-price"><a class="sort-by-container" href="#"><span>price</span></a></li>
                                    <li class="clearer visible-sms"></li>
                                    <li class="sort-by-rating active"><a class="sort-by-container" href="#"><span>rating</span></a></li>
                                    <li class="sort-by-popularity"><a class="sort-by-container" href="#"><span>popularity</span></a></li>
                                </ul>
                                
                                <ul class="swap-tiles clearfix block-sm">
                                    <li class="swap-list active">
                                        <a href="car-list-view.html"><i class="soap-icon-list"></i></a>
                                    </li>
                                    <li class="swap-grid">
                                        <a href="car-grid-view.html"><i class="soap-icon-grid"></i></a>
                                    </li>
                                    <li class="swap-block">
                                        <a href="car-block-view.html"><i class="soap-icon-block"></i></a>

                                    </li>

                                </ul>-->

                            </div>

                            <div class="car-list listing-style3 car">
                            <?php if($rs_count > 0){ ?>

                                <?php foreach ($rs_traslados as $traslado) { ?>

                                <article class="box">

                                    <figure class="col-xs-3">

                                        <span><img alt="" src="http://goguytravel.com/assets/images/<?php echo $traslado["Tipo_Traslado_Imagen"] ?>"></span>

                                    </figure>

                                    <div class="details col-xs-9 clearfix">

                                        <div class="col-sm-8">

                                            <div class="clearfix">

                                                <h4 class="box-title"><?php echo $traslado["Tipo_Traslado_Nombre"] ?></h4>

                                                <div class="operatedby">

                                                    <img class="img-responsive" width="110" src="http://goguytravel.com/assets/images/mdlogo-hor.png" alt="" />

                                                </div>

                                                <a href="#" data-toggle="modal" data-target="#modalTrans<?php echo $traslado["Id"] ?>"><i class="soap-icon-plus"></i> More Info</a>

                                            </div>

                                            <div class="amenities">

                                                <?php echo $traslado["Tipo_Traslado_Descripcion"] ?>

                                            </div>

                                        </div>

                                        <div class="col-xs-6 col-sm-2 character">

                                            <dl class="">

                                                <dt class="skin-color">Passengers</dt><dd><?php echo $info["pax"]."(Ad:".$info["adultos"]."/Ch:".$info["menores"].")"; ?> </dd>

                                                <dt class="skin-color">Pickup Time</dt><dd>5:45 pm</dd>

                                                <dt class="skin-color">Location</dt><dd><?php echo $ciudad["Ciudad"]; ?></dd>

                                            </dl>

                                        </div>

                                        <div class="action col-xs-6 col-sm-2">

                                            <span class="price"><small><?php echo $traslado["Divisa_Codigo"] ?></small>

                                            <?php echo number_format($traslado["Precio"], 2, '.', ',') ?></span>

                                            <form method="post" action="http://www.goguytravel.com/Booking/Transfers">

                                                <input type="hidden" name="traslado" value="<?php echo $traslado['Id']; ?>">

                                                <button type="submit" class="button btn-small full-width">BOOK</button>

                                            </form>

                                        </div>

                                    </div>

                                </article>
                                <?php } ?>
                            <?php }else{ ?>
                                <article class="box" style="text-align: center;">
                                    <i class="fa fa-frown-o fa-5x" aria-hidden="true"></i>
                                    <h3 style="text-transform: capitalize;">Apparently there is no shuttle service available that match with your search data</h3>
                                    <h4>Need Help? We Can Help You everytime! 1-800-000000</h4>
                                </article>
                              <?php  } ?>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section> 

        <?php foreach ($rs_traslados as $traslado) { ?>

        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="modalTrans<?php echo $traslado["Id"] ?>">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        <h4 class="modal-title" id="myModalLabel"><?php echo $traslado["Tipo_Traslado_Nombre"]." in ".$ciudad["Ciudad"]; ?></h4>

                    </div>

                    <div class="modal-body">

                        <div class="col-md-12">

                            <div class="col-md-4">

                                <img class="img-responsive" alt="" src="http://goguytravel.com/assets/images/<?php echo $traslado["Tipo_Traslado_Imagen"] ?>"></span>

                            </div>

                            <div class="col-md-8">

                                <h4><?php echo $traslado["Tipo_Traslado_Nombre"] ?></h4>

                                <p><?php echo $traslado["Tipo_Traslado_Descripcion"] ?></p>

                            </div>

                            <div class="col-md-4">

                                <b>

                                    What's included?    

                                </b>

                                <ul>

                                    <?php foreach ($inclusiones as $incl) {

                                        if($incl["Traslado_Tipo"] == $traslado["Tipo_Traslado_Id"]){

                                            if($incl["Activo"] == 1){

                                                if($incl["Costo_Extra"] != 0){

                                                    echo "<li><i class='soap-icon-check-1'></i> ".$incl["Inclusion"]."($)</li>";

                                                }else{

                                                    echo "<li><i class='soap-icon-check-1'></i> ".$incl["Inclusion"]."</li>";

                                                }

                                            }

                                        }

                                    } ?>

                                </ul>

                            </div>

                            <div class="col-md-8">

                            <b>Where we pick-up you?</b>

                            <div style="height: 120px;" id="map"></div>

                            </div>

                        </div>                        

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                        <button type="button" class="btn btn-primary">Book Now</button>

                    </div>

                </div>

            </div>

        </div>

        <?php } ?>

        <script>

            function initMap() {

                var myLatLng = {lat: -25.363, lng: 131.044};

                var map = new google.maps.Map(document.getElementById('map'), {

                    zoom: 2,

                    center: myLatLng

                });

                var marker = new google.maps.Marker({

                    position: myLatLng,

                    map: map,

                    title: 'Hello World!'

                });

            }

    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBER36l5Zz1M2o0TIpyqbuUztmTryGIyA8&signed_in=true&callback=initMap"></script>

