        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Your Booking Is Confirmed</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">Airport Transportation</a></li>
                    <li><a href="#">Book</a></li>
                    <li class="active">Confirmation</li>
                </ul>
            </div>
        </div>
        <section id="content" class="gray-area">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-sm-8 col-md-9">
                        <div class="booking-information travelo-box">
                            <h2>Booking Confirmation</h2>
                            <hr />
                            <div class="booking-confirmation clearfix">
                                <i class="soap-icon-check-1 icon circle"></i>
                                <div class="message">
                                    <h4 class="main-message">Thank You. Your Booking is Confirmed Now.</h4>
                                    <p>A confirmation email has been sent to your provided email address.</p>
                                </div>
                                <a href="#" class="button print-button btn-small uppercase">print Details</a>
                            </div>
                            <hr />
                            <h2>Traveler Information</h2>
                            <dl class="term-description">
                                <dt>Booking number:</dt><dd><?php echo $reserva["Conf_Id"]; ?></dd>
                                <dt>First name:</dt><dd><?php echo $cliente["Nombre"]; ?></dd>
                                <dt>Last name:</dt><dd><?php echo $cliente["Apellido"]; ?></dd>
                                <dt>E-mail address:</dt><dd><?php echo $cliente["Email"]; ?></dd>
                                <dt>Service:</dt><dd><?php echo $reserva["Servicio_Nombre"]; ?></dd>
                                <dt>Airport:</dt><dd><?php echo $aeropuerto["Airport"]; ?></dd>
                                <dt>Hotel:</dt><dd><?php echo $reserva["Hotel"]; ?></dd>
                                <dt>Dates:</dt><dd><?php echo date("M", strtotime($reserva["Fecha_Llegada"]))." ".date("d", strtotime($reserva["Fecha_Llegada"])).", ".date("Y", strtotime($reserva["Fecha_Llegada"]))." TO ".date("M", strtotime($reserva["Fecha_Regreso"]))." ".date("d", strtotime($reserva["Fecha_Regreso"])).", ".date("Y", strtotime($reserva["Fecha_Regreso"]))?></dd>
                            </dl>
                            <hr />
                            <h2>Payment Information</h2>
                            <dl class="term-description">
                                <dt>Service Price:</dt><dd>$<?php echo number_format($reserva["Costo"], 2, '.', ','); ?></dd>
                                <dt>Total Amount of the Reservation:</dt><dd>$<?php echo number_format($reserva["Costo"], 2, '.', ','); ?></dd>
                                <dt>Payment Type:</dt><dd><?php echo $reserva["Tipo_Pago"]==14?"Pay at the Arrive":"Other"; ?></dd>
                                <dt>Payed:</dt><dd>NO</dd>
                                <dt>Amount Due:</dt><dd>$<?php echo number_format($reserva["Costo"], 2, '.', ','); ?></dd>
                                <p>You select the payment method "Pay at the arrival", this means that your booking is confirmed but not is payed, at the moment of your arrival our representants are waiting for you, they help you to go to your vehicle and complete the payment.</p>
                            </dl>
                            <hr />
                            <h2>View Booking Details</h2>
                            <p>Your confirmation voucher is needed for provide the service at your arrival you can print it, or in your movil device, you can view it clicking the next link.</p>
                            <br />
                            <a href="#" class="red-color underline view-link">https://www.goguytravel.com/booking-details/?=f4acb19f-9542-4a5c-b8ee</a>
                        </div>
                    </div>
                    <div class="sidebar col-sm-4 col-md-3">
                        <div class="travelo-box contact-box">
                            <h4>Need Go Guy Help?</h4>
                            <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br>
                                <a class="contact-email" href="#">help@goguytravel.com.com</a>
                            </address>
                        </div>
                        <div class="travelo-box book-with-us-box">
                            <h4>Why Book with Go Guy?</h4>
                            <ul>
                                <li>
                                    <i class="soap-icon-heart circle"></i>
                                    <h5 class="title"><a href="#">We Care You</a></h5>
                                    <p>From your flght booking, to your tours at the destiny we can take care of you and your vacations.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-savings circle"></i>
                                    <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                                    <p>The best offers with the best price, we offer you promotions all the year.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-support circle"></i>
                                    <h5 class="title"><a href="#">Excellent Support</a></h5>
                                    <p>Our Customer Service deparment are ready to help you making a unique and full customized travel experience.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>