<div class="page-title-container">

            <div class="container">

                <div class="page-title pull-left">

                    <h2 class="entry-title">Transfer Booking</h2>

                </div>

            </div>

        </div>

        <section id="content" class="gray-area">

            <div class="container">

                <div class="row">

                    <div id="main" class="col-sms-6 col-sm-8 col-md-9">

                        <div class="booking-section travelo-box">

                            

                            <form class="booking-form" id="clientinfo" method="post" action="/Confirmation/Transfers">

                                <div class="person-information">

                                    <h2>Your Personal Information</h2>

                                    <div class="form-group row">

                                        <div class="col-sm-6 col-md-5">

                                            <label>First name</label>

                                            <input type="text" class="input-text full-width" name="Name" value="" placeholder="" />

                                        </div>

                                        <div class="col-sm-6 col-md-5">

                                            <label>Last name</label>

                                            <input type="text" class="input-text full-width" name="Lastname" value="" placeholder="" />

                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <div class="col-sm-6 col-md-5">

                                            <label>email address</label>

                                            <input type="text" class="input-text full-width" name="email" value="" placeholder="" />

                                        </div>

                                        <div class="col-sm-6 col-md-5">

                                            <label>Verify E-mail Address</label>

                                            <input type="text" class="input-text full-width" name="verifyemail" value="" placeholder="" />

                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <div class="col-sm-6 col-md-5">

                                            <label>Country code</label>

                                            <div class="selector">

                                                <select class="full-width">

                                                    <option>United Kingdom (+44)</option>

                                                    <option>United States (+1)</option>

                                                </select>

                                            </div>

                                        </div>

                                        <div class="col-sm-6 col-md-5">

                                            <label>Phone number</label>

                                            <input type="text" class="input-text full-width" name="phone" value="" placeholder="" />

                                        </div>

                                    </div>

                                </div>

                                <hr />

                                <h2>Flights Information</h2>

                                <div class="form-group row">

                                    <div class="col-sm-6 col-md-5">

                                        <label>Arrival Flight Airline</label>

                                        <input type="text" name="arrival_airline" class="input-text full-width">

                                    </div>

                                    <div class="col-sm-6 col-md-5">

                                        <label>Arrival Flight Number</label>

                                        <input type="text" name="arrival_flight" class="input-text full-width">

                                    </div>

                                    <div class="col-sm-6 col-md-5">

                                        <label>Departure Flight Airline</label>

                                        <input type="text" name="departure_airline" class="input-text full-width">

                                    </div>

                                    <div class="col-sm-6 col-md-5">

                                        <label>Departure Flight Number</label>

                                        <input type="text" name="departure_flight" class="input-text full-width">

                                    </div>

                                </div>

                                <hr>

                                <div class="card-information">

                                    <h2>How would You like to pay?</h2>

                                    <div class="form-group row">

                                        <div class="col-sm-6 col-md-5 radio">

                                            <label>

                                                <input type="radio" name="payment" id="payarrival" value="1">

                                                Pay at your arrival.

                                            </label>

                                        </div>

                                        <div class="col-sm-6 col-md-5 radio">

                                            <label>

                                                <input type="radio" name="payment" id="card" value="2" disabled="disabled">

                                                Debit/Credit Card (Coming soon).

                                            </label>

                                        </div>

                                    </div>

                                </div>

                                <hr />

                                <div class="form-group">

                                    <div class="checkbox">

                                        <label>

                                            <input type="checkbox" name="terms"> By continuing, you agree to the <a href="#"><span class="skin-color">Terms and Conditions</span></a>.

                                        </label>

                                    </div>

                                </div>

                                <div class="form-group row">

                                    <div class="col-sm-6 col-md-5">

                                        <button type="submit" class="full-width btn-large">CONFIRM BOOKING</button>

                                    </div>

                                </div>

                                <hr>

                            </form>

                        </div>

                    </div>

                    <div class="sidebar col-sms-6 col-sm-4 col-md-3">

                        <div class="booking-details travelo-box">

                            <h4>Transportation Details</h4>

                            <article class="car-detail">

                                <figure class="clearfix">

                                    <a title="" href="car-detailed.html" class="middle-block"><img class="middle-item" alt="" src="http://goguytravel.com/assets/images/<?php echo $traslado['Tipo_Traslado_Imagen'] ?>"></a>

                                    <div class="travel-title">

                                        <h5 class="box-title">

                                            <?php echo $traslado["Tipo_Traslado_Nombre"]; ?>

                                            <small><?php echo $ciudad["Ciudad"]; ?></small>

                                        </h5>
                                    </div>

                                </figure>

                                <div class="details">

                                    <div class="icon-box style11 full-width">

                                        <div class="icon-wrapper">

                                            <i class="soap-icon-departure"></i>

                                        </div>

                                        <dl class="details">

                                            <dt class="skin-color">Date</dt>

                                            <dd><?php echo date("M", strtotime($info["llegada"]))." ".date("d", strtotime($info["llegada"])).", ".date("Y", strtotime($info["llegada"]))." TO ".date("M", strtotime($info["regreso"]))." ".date("d", strtotime($info["regreso"])).", ".date("Y", strtotime($info["regreso"]))?></dd>

                                        </dl>

                                    </div>

                                    <div class="icon-box style11 full-width">

                                        <div class="icon-wrapper">

                                            <i class="soap-icon-departure"></i>

                                        </div>

                                        <dl class="details">

                                            <dt class="skin-color">From</dt>

                                            <dd><?php echo $aeropuerto["Airport"]." (".$aeropuerto["Iata"].")"; ?></dd>

                                        </dl>

                                    </div>

                                    <div class="icon-box style11 full-width">

                                        <div class="icon-wrapper">

                                            <i class="soap-icon-departure"></i>

                                        </div>

                                        <dl class="details">

                                            <dt class="skin-color">To</dt>

                                            <dd><?php echo $info["hotel"]; ?></dd>

                                        </dl>

                                    </div>

                                </div>

                            </article>

                            

                            <h4>What's include?</h4>

                            <dl class="other-details">

                                <?php

                                 foreach($inclusion as $incl) { ?>

                                <dt class="feature"><?php echo $incl["Inclusion"] ?></dt>

                                <dd class="value"><?php echo $incl["Costo_Extra"]==0?"FREE":"($)"; ?></dd>

                                <?php } ?>

                            </dl>

                            <br>

                            <br>

                            <h4>What´re you paying?</h4>

                            <dl class="other-details">

                                <dt class="feature">Transportation Service</dt>

                                <dd class="value"><?php echo "$".number_format($traslado["Precio"], 2, '.', ','); ?></dd>

                                <dt class="feature">Total</dt>

                                <dd class="value"><?php echo "$".number_format($traslado["Precio"], 2, '.', ','); ?></dd>

                            </dl>

                        </div>

                        

                        <div class="travelo-box">

                            <h4><a href="#">Need help?</a></h4>

                        </div>

                    </div>

                </div>

            </div>

            <p>.</p>

        </section>