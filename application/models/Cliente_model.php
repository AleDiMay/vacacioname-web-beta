<?php
class Cliente_model extends CI_Model {

	public function __construct()
    {
      parent::__construct();
    }
  function Add($session, $ip, $navegador, $plataforma){
    $this->load->dbutil(); 
    $insert = $this->db->query("INSERT INTO clientes (Session_Id, Fecha, Ip, Navegador, Plataforma) VALUES ('".$session."', ".date("Y-m-d").", '".$ip."', '".$navegador."', '".$plataforma."')");
    $query = $this->db->insert_id();
    return $query;
  }
  function AddProductTransfer($cliente, $id_servicio, $nombre_servicio, $hotel, $tipo_pago, $pagado, $total, $fecha_arrivo, $fecha_regreso, $pax_adultos, $pax_menores){
    $data = array('client' => $cliente,
                  'Id_Servicio' => $id_servicio,
                  'Servicio_Nombre' => $nombre_servicio,
                  'Hotel' => $hotel,
                  'Tipo_Pago' => $tipo_pago,
                  'Pagado' => $pagado,
                  'Costo' => $total,
                  'Fecha_Llegada' => $fecha_arrivo,
                  'Fecha_Regreso' => $fecha_regreso,
                  'Pax_Adultos' => $pax_adultos,
                  'Pax_Menores' => $pax_menores
                 );
    $this->db->insert('Reservaciones_Transportacion', $data);
    $query = $this->db->insert_id();
    return $query;
  }
  function GetReservaTraslado($reserva){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM Reservaciones_Transportacion WHERE Id =".$reserva);
    return $query;
  }
  function GetReservaByConfirmation($reserva){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM Reservaciones_Transportacion WHERE Conf_Id = '".$reserva."'");
    return $query;
  }
  function GetClienteById($cliente){
    $this->load->dbutil();
    $query = $this->db->query("SELECT * FROM clientes WHERE Id =".$cliente);
    return $query;
  }
  function ConfirmReservationTransfer($reserva, $aerolinea_llegada, $aerolinea_regreso, $vuelo_llegada, $vuelo_regreso, $pax){    
    $this->load->dbutil();
    //Construct Confirmation Number}
    $pax = $pax;
    $rndm = random_string('numeric', 4);
    $confirmation_number = "GG".$pax."6".$rndm."R"."23"."P"."14";

    $reservaData = array(
                  'Aerolinea_Llegada' => $aerolinea_llegada,
                  'Aerolinea_Regresa' => $aerolinea_regreso,
                  'Vuelo_Llegada' => $vuelo_llegada,
                  'Vuelo_Regresa' => $vuelo_regreso,
                  'Conf_Id' => $confirmation_number
                 );
    $this->db->where('Id', $reserva);
    $this->db->update('Reservaciones_Transportacion', $reservaData);

    $reservaInfo = $this->GetReservaTraslado($reserva);
    return $reservaInfo;
  }
  function UpdateClientData($cliente, $nombre, $apellido, $email, $telefono){
    $clientData = array(
                  'Nombre' => $nombre,
                  'Apellido' => $apellido,
                  'Email' => $email,
                  'Telefono' => $telefono
                 );
    $this->db->where('Id', $cliente);
    $this->db->update('clientes', $clientData);
    $clienteInfo = $this->GetClienteById($cliente);
    return $clienteInfo;
  }

}
?>