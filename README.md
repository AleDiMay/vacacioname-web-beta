# Vacacioname Web Beta #

*Debido al tiempo de desarrollo, y la finalización de proyecto muchas partes se encuentran inconclusas*.
Aplicación Web Desarrollada sobre PHP, Sobre Framework Code Igniter 2.x, orientado la administración de productos, asi como la presentación al usuario final (Por motivos relacionados con tiempo y costo de producción se precedió de una interfaz "API" entre Front y Back End, funcionando todo sobre la misma plataforma)

### ¿De que es capaz la aplicación? ###

* Manejo de los diversos productos (Traslados, Tours, Hoteles)
* Flexible para implementación futura de consumo de datos de API's de el rubro (PriceTravel, BD Travel, Etc).
* Presentación al usuario del sistema de Booking Online, asi como integración con diversas pasarelas de pago.

### Tecnologías Aplicadas ###

* PHP
* Code Igniter 2.x
* MariaDB
* Procedimientos Almacenados y Base de Datos Relacional
* Google Cloud Platform